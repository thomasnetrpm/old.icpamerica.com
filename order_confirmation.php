<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

include 'classes/Coupon.php';
include 'classes/Tax.php';
include 'classes/Administrator.php';
?>
<?php
$client_id = $_SESSION['client_id'];

$clients = Billing::findBillingClient($client_id);
$billing  = Billing::findBillingClient($client_id);
$shipping = Shipping::findShippingClient($client_id);

if ($_REQUEST['back_previous']==1) {
  header("Location: ".$root."billing-info.html");
  exit();
}


if(isset($_POST['submit_order'])) {
  
  $_POST = sanitize($_POST);
  $client = $_POST;
  settype($client,'object');

  $date = date('Y-m-d');
  $order_code = $client_id .'-'.date('Ymd').'-'.rand(1,400);
  $status = 'New';
  $condition = "fldTempCartClientID='$client_id' AND fldTempCartDate='$date'";
        
  $cart = Tempcart::findTempcartByCondition($condition);
  foreach($cart as $carts) {
    //get all info of tempcart
    $_POST = sanitize($_POST);
    $myCart = $_POST;
    settype($myCart,'object');
    
    $myCart->fldCartProductID = $carts->fldTempCartProductID;
    $myCart->fldCartClientID = $carts->fldTempCartClientID;
    $myCart->fldCartProductName = $carts->fldTempCartProductName;
    $myCart->fldCartProductVariant = $carts->fldTempCartProductVariant;
    $myCart->fldCartProductPrice = $carts->fldTempCartProductPrice;
    $myCart->fldCartProductPriceText = $carts->fldTempCartProductPriceText;
    $myCart->fldCartQuantity =$carts->fldTempCartQuantity;
    $myCart->fldCartOrderNo = $order_code;        
    $myCart->fldCartStatus = $status;
    //save to cart
    Cart::addCart($myCart);               
  }
  
  //save tax
  $stateTax = State::findState($billing->fldBillingState);
  $taxAmount = $_POST['subtotal'] * ($stateTax->fldStateTax);
  // $total = $_POST['subtotal'] + $taxAmount;

  $tax = $taxAmount;
  Tax::addTax($order_code,$tax);

  // Update Cart with Coupon
  Cart::updateCartXCoupon($order_code, $client);
  
  // remove order from tempcart
  Tempcart::deleteTempCartByCondition($condition);
  
  $clients = Billing::findBillingClient($client_id);
  $billing  = Billing::findBillingClient($client_id);
  $shipping = Shipping::findShippingClient($client_id);
  
  //send email order to CLIENT
  require_once("includes/email_receipt.php");
  
  $links = $root."thank_you/default.html";
  header("Location: $links");
  exit();
}?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->
<!--   	<hgroup>
      <h1>Order Confirmation - needs payment gateway</h1>
      <hr />
    </hgroup> -->

    <? // require_once("includes/order_confirmation.php");?>
  
  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li>Order Confirmation</li>
    </ul>

    <form action="" method="post" class="row-fluid" name="bill_form">
      <table class="table table-bordered cart-order-list">
          <thead>
              <tr class="cart-hdr">
                  <th class="hdr-panel5"></th>
                  <th class="hdr-panel1">Item Name</th>
                  <th class="hdr-panel2">Price</th>
                  <th class="hdr-panel3">QTY</th>
                  <th class="hdr-panel4">Item Total</th>
              </tr>
          </thead>
          <tbody>
              <?
                  $date = date('Y-m-d');
                  $condition = "fldTempCartClientID='$client_id' AND fldTempCartDate='$date'";
                  $cart = Tempcart::findTempcartByCondition($condition);
                  $total = 0;
                  foreach($cart as $carts) { 
                      $products = Products::findProducts($carts->fldTempCartProductID);
                      $subtotal = $carts->fldTempCartProductPrice * $carts->fldTempCartQuantity;
                      $total = $total + $subtotal;
                      $productPriceText = $carts->fldTempCartProductPriceText;

                      $corporate_price_text = $products->fldProductsPrice2Text;

                      // Product Option + Variant
                      $variant_id = $carts->fldTempCartProductVariant;
                      if (!empty($variant_id)) {
                        $variant = ProductVariant::findVariant($variant_id);
                        $variant_name = $variant->product_variant;
                        $option_id = $variant->product_option_id;
                        $option = ProductVariant::findOption($option_id);
                        // $option_name = $option->product_option;

                        $option_name = ($option->product_option=="Product Options")? "": $option->product_option.' | ';
                      }
              ?>
              <tr>
                  <td class="tp5">&nbsp;</td>
                  <td class="tp1">
                    <?php // Show no image to accessories
                    if (strtolower($option->product_option)!='accessories') {
                      ?>
                      <img src="<?=$root?>uploads/product_image/<?=$carts->fldTempCartProductID?>/_75_<?=$products->fldProductsImage?>" alt="" border=0 align="left"  width="75">
                      <?
                    } else {
                      ?>
                      <img src="<?=$root?>uploads/product_image/thumbblank.jpg" alt="" border=0 align="left"  width="75">
                      <?
                    }
                    ?>
                      &nbsp;&nbsp;&nbsp;
                      <?=$carts->fldTempCartProductName?>
                      <?php if ($productPriceText) { echo "<br>&nbsp;&nbsp;&nbsp;<b>** ".$productPriceText." **</b>"; }?>
                      <?php if ($variant_id) { 
                        echo "<br>&nbsp;&nbsp; ".$option_name.$variant_name." "; 
                        if ($_SESSION['client_type']==2) { // Corporate
                          echo "&nbsp;&nbsp;&nbsp;<b>** ".$corporate_price_text." **</b>";
                        }
                      }?>
                  </td>
                  <td class="tp2"><?=number_format($carts->fldTempCartProductPrice,2)?></td>
                  <td class="tp3"><?=$carts->fldTempCartQuantity?></td>
                  <td class="tp4"><?=number_format($subtotal,2)?></td>
              </tr>
                  <? } ?>
          </tbody>
          <tfoot>
              <tr class="cart-ftr">
                  <td colspan="4" align="right">Sub Total</td>
                  <td class="total-cart-price">$ <?=number_format($total,2)?></td>
              </tr>
              <input type="hidden" name="subtotal" value="<?=$total?>">

              <?php
              // $totalAmount = $total;
              // Initial Tax refers to default State
              $billingState = ($billing->fldBillingState!='')? $billing->fldBillingState: 'CA';
              if($billingState != "") {            
                $stateTax = State::findState($billingState);           
                $taxAmount = $total * ($stateTax->fldStateTax);
                $total = $total + $taxAmount;
              }
              ?>
              <tr class="cart-ftr">
                  <td colspan="4" align="right">Estimated Tax (<?=$billingState?>)</td>
                  <td class="total-cart-price">$ <?=number_format($taxAmount,2)?></td>
                  <input type="hidden" name="tax" value="<?=$taxAmount?>" id="taxvalue" />
              </tr>

              <?php
              if(isset($_POST['update'])) {
                  // Update Cart with coupon
                  if (isset($_POST['coupon_code'])) {
                      $coupon_code = trim($_POST['coupon_code']);
                      // Check 
                      $cou = Coupon::findCouponByCode($coupon_code);
                      $coupon_price   = $cou->fldCouponPrice;
                      $coupon_percent = $cou->fldCouponPercent;
                      $coupon_freeship= $cou->fldCouponFreeShipping;

                      if ($coupon_price > 0) {
                          $coupon_amount = $coupon_price;
                      } elseif ($coupon_percent > 0) {
                          $coupon_amount = $total * ($coupon_percent / 100);
                      } elseif ($coupon_freeship > 0) {
                          // Get shipping amount
                          echo 'get shipping amount<br>';
                      }

                      if ($coupon_amount == '' || $coupon_amount == '0') {
                        $coupon_message = '<br> <small>*Sorry but we can not find the code you entered.</small>';
						$coupon_code = '';
                      } else {
                        $coupon_message = '<br> <small>*You can only use one discount code at a time</small>';
                      }
                      // $saveCoupon = TempCart::
                  }
              }

              $grandTotal = $total - $coupon_amount;
              ?>
              <tr>
                  <td colspan="2" align="right">
                    Discount Code
                    <?=$coupon_message?>
                    <br>
                    <button name="update" type="submit" id="update" class="btn btn-small" value="1">Click to apply</button>
                  </td>
                  <td colspan="2"> <input type="text" name="coupon_code" value="<?=$coupon_code?>"> </td>
                  <td class="total-cart-price">$ <?=number_format($coupon_amount,2)?></td>
              </tr>
              <tr>
                  <td colspan="4" align="right"><strong>Grand Total</strong></td>
                  <td class="total-cart-price payment">$ <?=number_format($grandTotal,2)?></td>
              </tr>
              <tr>
                  <td colspan="5" class="cart-functions">
                      <input type="hidden" name="client_id" value="<?=$_SESSION['client_id']?>">
                      <input type="hidden" name="Id" value="<?=$billing->fldBillingID?>">
                      <input type="hidden" name="coupon_amount" value="<?=$coupon_amount?>">
                      <div class="btn-group">
                        <button name="back_previous" type="submit" id="back_previous" class="btn btn-small" value="1">Back to Previous page</button>
                        <button name="submit_order" type="submit" id="submit_order" class="btn btn-small btn-success" value="1">Submit Order</button>
                      </div>
                  </td>
              </tr>
          </tfoot>
      </table>      
    </form>
  </section>
  
  </article>
	<? endblock() //end section ?>


<? startblock('headercodes') ?>
<? endblock() //end headercodes ?>

<? startblock('extracodes') ?>
<? endblock() //end extracodes ?>
