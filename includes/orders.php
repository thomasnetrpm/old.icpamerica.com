    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td >
          <div align="center" style="width:650px;margin:auto;border-bottom:solid 1px #a0a0a0;box-shadow:0 1px 0 0 #f0f0f0;">
            <table border="0" cellpadding="0" cellspacing="0" width="650" style="font-size:12px;">
              <tr>
                <td colspan="2" align="center" width="100%"> <a href="http://50.62.42.235/~icpameri/"> <img src="http://50.62.42.235/~icpameri/assets/images/hdr-logo.png" width="242" height="100" alt="ICP America"> </a> </td>
              </tr>
              <tr>
                <td width="50%" style="background:#427700;font-weight:600;color:#FFF;text-align:left;padding:10px;"> Order No : %%order_no%% </td>
                <td width="50%" style="background:#427700;font-weight:600;color:#FFF;text-align:right;padding:10px;"> Order Date : %%order_date%% </td>
              </tr>
            </table>
          </div>
          <!-- HEADER PANEL -->

          <div align="center" style="background:rgba(0,0,0,0.10);width:650px;margin:auto;padding:10px 0;border-bottom:solid 1px #a0a0a0;box-shadow:0 1px 0 0 #f0f0f0;">
            <table border="0" cellpadding="0" cellspacing="0" width="650">
              <tr>
                <td width="50%" style="font:600 13px sans-serif;color:#555;text-transform:uppercase;text-shadow:none;padding:10px 20px;"> Billing Information </td>
                <td width="50%" style="font:600 13px sans-serif;color:#555;text-transform:uppercase;text-shadow:none;padding:10px 20px;"> Shipping Information </td>
              </tr>

              <tr>
                <td style="font:300 13px sans-serif;color:#555;text-shadow:none;line-height:18px;padding:10px 20px;">
                  %%name%% <br>
                  %%address%% <br>
                  <a href="mailto:%%email%%" style="color:#666;text-decoration:none;border-bottom:dotted 1px #333;">%%email%%</a> <br>
                  %%contact%%
                </td>
                <td style="font:300 13px sans-serif;color:#555;text-shadow:none;line-height:18px;padding:10px 20px;">
                  %%shipping_name%% <br>
                  %%shipping_address%% <br>
                  <a href="mailto:%%shipping_email%%" style="color:#666;text-decoration:none;border-bottom:dotted 1px #333;">%%shipping_email%%</a> <br>
                  %%shipping_contact%%
                </td>
              </tr>
            </table>
          </div>
          <!-- INFO PANEL -->

         %%orders%%
          <!-- ORDERS PANEL -->
        </td>
      </tr>
    </table>
