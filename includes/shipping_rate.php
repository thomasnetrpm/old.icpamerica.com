<?
	if(isset($_SESSION['client_id'])) {
		$client_id = $_SESSION['client_id'];
	} else {
		$links = $ROOT_URL;
		header("Location: $links");
	}
	
	
		
?>
<style>
	td {padding:5px 5px;}
</style>
<p>&nbsp;</p>	<p>&nbsp;</p>	
<form method="post" action="<?=$ROOT_URL?>order-confirmation.html">
<table width="713" border="0">
<tr>
	<td width="361">
    	<table width="347" border="0" cellpadding="1" cellspacing="1" bgcolor="#666666">
        	<tr>
            	<td height="25" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF">Billing Information</td>                
          </tr>
          <? 
		  	$client = Billing::findBillingClient($client_id);
		  ?>
          <tr bgcolor="#FFFFFF">
          	<td>
            	<table border="0">
                	<tr>
                        <td width="96" height="25">Name</td>
         	            <td width="11" height="25">:</td>
                        <td width="215" height="25"><?=$client->fldBillingFirstName . ' ' . $client->fldBillingLastname?></td>
		             </tr>
                     <tr>
                        <td height="25">Address</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$client->fldBillingAddress . ' ' . $client->fldBillingAddress1 . ' ' . $client->fldBillingCity . ' ' . $client->fldBillingState . ' ' . $client->fldBillingCountry?></td>
		             </tr>
                     <tr>
                        <td height="25">Contact No</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$client->fldBillingPhoneNo?></td>
		             </tr>
                     <tr>
                        <td height="25">Email Address</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$client->fldBillingEmail?></td>
		             </tr>
                </table>
            </td>
          </tr>    
        </table>
    </td>
    <td width="369">
    	<table border="0" cellpadding="1" cellspacing="1" bgcolor="#666666">
        	<tr>
            	<td width="328" height="25" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF">Shipping Information</td>                
          </tr>
          <tr bgcolor="#FFFFFF">
          	<td>
             <? $shipping = Shipping::findShippingClient($client_id);?>
            	<table border="0">
                	<tr>
                        <td height="25">Name</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$shipping->fldShippingFirstName . ' ' . $shipping->fldShippingLastname?></td>
		             </tr>
                     <tr>
                        <td height="25">Address</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$shipping->fldShippingAddress . ' ' . $shipping->fldShippingAddress1 . ' ' . $shipping->fldShippingCity . ' ' . $shipping->fldShippingState . ' ' . $shipping->fldShippingCountry?></td>
		             </tr>
                     <tr>
                        <td height="25">Contact No</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$shipping->fldShippingPhoneNo?></td>
		             </tr>
                     <tr>
                        <td height="25">Email Address</td>
         	            <td height="25">:</td>
                        <td height="25"><?=$shipping->fldShippingEmail?></td>
		             </tr>
                </table>
            </td>
          </tr>    
        </table>
    </td>
</tr>

</table>
<p>&nbsp;</p>
				<div id="blank">
					
                    
					<table width="100%" style="text-align:center;" bgcolor="#FFFFFF" cellpadding="0" cellspacing="1">

											
<tr style="background-color:#666666;color:#FFF; height:30px">
												
											
												<td align=center width="70%"><font face="Arial, Helvetica, sans-serif" size="2"><B>Item Name</B></font></td>
												<td align=right nowrap width="10%"><font face="Arial, Helvetica, sans-serif" size="2"><B>Price</B> ($)</font></td>
												<td align=center width="10%"><B><font face="Arial, Helvetica, sans-serif" size="2">Quantity</font></B></td>
												<td align=right nowrap width="10%"><font face="Arial, Helvetica, sans-serif" size="2"><B>Item Total</B> ($)</font></td>
											</tr>
                                            <? 
												$date = date('Y-m-d');
												$condition = "fldTempCartClientID='$client_id' AND fldTempCartDate='$date'";
												$cart = Tempcart::findTempcartByCondition($condition);
												if(Tempcart::countTempcartbyCondition($condition)==0) {
											?>
                                            <tr>
                                            	<td colspan="5" class="error">Your shopping cart is empty</td>
                                            </tr>
											<?
												} else {
													foreach($cart as $carts) {
													$trClass = ($row_ctr%2==0) ? 'bgcolor="#F5F5F5"' : 'bgcolor="#FFFFFF"';
													
											?>
													
													<tr <?=$trClass?>>
														

														<td valign="middle" align="center">
														<?
															$products = Products::findProducts($carts->fldTempCartProductID);
															 $weight = $weight + $products->fldProductsWeight;
															
									
														?>
														<img src="<?=$ROOT_URL?>products_image/<?=$products->fldProductsImage?>" alt="" border=0 align="left"  width="75">
														
															
														<table border="0">
                                                        <tr><td align="left">
														<B><font face="Arial, Helvetica, sans-serif" size="2"><?=$carts->fldTempCartProductName?></font></B><br />                              
                                                         <? 
															if($carts->fldTempCartOptions != "") {
																$option = explode(',',$carts->fldTempCartOptions);
																foreach($option as $options) {
																	$pOptions = ProductsOption::findProductsOptionCart($options);
																	$nOptions = Options::findOptions($pOptions->fldProductsOptionMainId);
																	$cOptions = OptionsCategory::findOptionsCategory($pOptions->fldProductsOptionCategoryId);
																	$optionAmount = $optionAmount + $pOptions->fldProductsOptionAmount;
																
													    ?>
                                                        	<strong><?=$cOptions->fldOptionsCategoryName?> : </strong> <?=$nOptions->fldOptionsName?>  - (+) $ <?=number_format($pOptions->fldProductsOptionAmount,2)?><br />
                                                        <?			
																
															} }
															
														?>
                                                        </td></tr>																																																																																							                                                                                                               	                                                        
                                                  </table>
															
															
																</td>
														<td
															onmousedown="setCheckboxRow('document.item_form.cbid','<?=$row_ctr?>');"
															 align=right><font face="Arial, Helvetica, sans-serif" size="2"><?=number_format($carts->fldTempCartProductPrice,2)?></font></td>
	 															
															
														<td align=center><font class=default><?=$carts->fldTempCartQuantity?> 
                                                             <input type="hidden" name="cartId[]" value="<?=$carts->fldTempCartID?>" />
                                                             </td>
															
														<td align=right>
                                                        <font face="Arial, Helvetica, sans-serif" size="2">
                                                        	<?
																$subtotal = $carts->fldTempCartProductPrice * $carts->fldTempCartQuantity;
																$subtotal = $subtotal + $optionAmount;
																echo number_format($subtotal,2);
															?>
                                                          </font>
														</td>
													</tr>
													<?
													$grandtotal += $subtotal;													
													$row_ctr++;
												}

												}
												?>
                                                <?
													if($client->fldBillingState == "CA") {
														$stateTax = "CA";
													} else if($shipping->fldShippingState == "CA") {
														$stateTax = "CA";
													} else {
														$stateTax = "";
													}
												?>
                                               <? if($stateTax == "CA") { ?>
												<tr style="background-color:#666666;color:#FFF; height:30px">
													<td colspan=3 align="right"><font color="#FFFFFF">
														<B><font face="Arial, Helvetica, sans-serif" size="2">Tax ($) :</font></B>
													</font></td>
													<td align=right><B>
                                                    <font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">
														<?
															//$tax = $grandtotal * .0925;
															$tax = $grandtotal * .0775;
															echo ' (+) '.number_format($tax,2);
															$grandtotal = $grandtotal + $tax;
														?>
                                                        </font>
													</B></td>
												</tr>
                                             <? } ?>   
<tr style="background-color:#666666;color:#FFF; height:30px">
													<td colspan=3 align="right"><font color="#FFFFFF">
														<B><font face="Arial, Helvetica, sans-serif" size="2">Sub Total ($) :</font></B>
													</font></td>
													<td align=right><B>
                                                    <font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">
														<?
															echo number_format($grandtotal,2);
														?>
                                                        </font>
													</B></td>
												</tr>

		      </table>
                          
<!-- 					<br />
					If you have a coupon code you can enter it below:                    
					<br />
                    Coupon Code: <input type='textbox' name='coupon_code' style="border: 1px solid #000;">
					<br /> -->
					<br />
                    <input type="hidden" name="grandtotal" value="<?=$grandtotal?>" />
                    <input name="continue_confirmation" type="submit" id="continue" value="Continue" />
					<br />
<div class="cls"></div>

					

				</div>
                </form>