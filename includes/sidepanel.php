<aside>

<!--

	<dl id="cart" class="side-framebox clearfix">
		<dt>Cart</dt>
		<dd>
        	<? if(isset($_SESSION['logged_in'])) { ?>
			<form action="<?=$root?>shopping-cart.html" method="post">
		        <? 
				$date = date('Y-m-d');
				$condition = "fldTempCartClientID='".$_SESSION['client_id']."' AND fldTempCartDate='$date'";

				$cart = Tempcart::findTempcartByCondition($condition);

				if(Tempcart::countTempcartbyCondition($condition)==0) {
					?>
					<span>No Products Selected</span>
					<?php
				} else {
					foreach($cart as $carts) {
						$cartID 	= $carts->fldTempCartID;
						$prodID 	= $carts->fldTempCartProductID;
						$name 		= $carts->fldTempCartProductName;
						$price 		= $carts->fldTempCartProductPrice;
						$quantity 	= $carts->fldTempCartQuantity;
						$subtotal	= number_format(($price * $quantity), 2);
						$subtotal_raw = ($price * $quantity);
						$total		+= $subtotal_raw;

						// Product Option + Variant
						$variant_id = $carts->fldTempCartProductVariant;
						if (!empty($variant_id)) {
							$variant = ProductVariant::findVariant($variant_id);
							$variant_name = $variant->product_variant;
							$option_id = $variant->product_option_id;
							$option = ProductVariant::findOption($option_id);
							// $option_name = $option->product_option;
							$option_name = ($option->product_option=="Product Options")? "": $option->product_option.' | ';

						}
						?>
						<small><?=$quantity.' | '.$name.' <br> '.$option_name.$variant_name.' | $'.$subtotal?></small><br>
						<?php
					}
				}
				?>
				<ul class="unstyled clearfix">
					<li class="pull-left">Total:</li>
					<li class="pull-right">$<?=number_format($total, 2)?></li>
				</ul>
				<button class="submit" class="btn">View Cart</button>
			</form>				
			<? } else { ?>
			<p align="center">
				<a href="<?=$root?>registration.html"><em>Sign Up</em></a><br>or<br>
				<a href="<?=$root?>login.html" class="login">login</a>
				Questions?<br>Email us at <a href="mailto:support@icpamerica.com">support@icpamerica.com</a><br>for online technicalsupport or call<br>1-877-293-2000 to order.				
			</p>
			<? } ?>
		</dd>
	</dl>

	-->
    
   

	<dl id="category" class="side-framebox clearfix noborder">
 
     
	<dt>Category</dt>

	<div class="nav-wrapper">
		<?php
		$category = Category::displayAllCategoryNotHidden();
		$ii = 1;
		
		foreach ($category as $catR) {

			$catName 	= $catR->fldCategoryName;
			$catid 		= $catR->fldCategoryID;
			$catNameURL = $catR->fldCategoryURL;
			$isOutsideURL = $catR->fldCategoryIsUrl;
			if ($catid == $categoryID) { $on = 'class="on"'; $slide = $ii;} else { $on = 'class="off"'; }

			if ($isOutsideURL==1) {
				?>
<!-- 			<dd><span class="single-nav"><a href="<?=$catNameURL?>" target='_blank'><?=$catName?></a></span></dd> -->
	        <div id="cat<?=$catid?>" class="single-nav-wrap">
		        <a href="<?=$catNameURL?>" target='_blank' class="link-left"><?=$catName?></a>
				<!--<a class="accordion-toggle link-arrow" data-toggle="collapse" data-parent="#accordion2" href="#demo" id="arrow"></a>-->
	        </div>
				<?
			} else {
				if ($catid == $categoryID) {
					$cataSelection = "link-white";
					$catSelection = "single-hover single-nav-wrap";
					$accordionSelection = "link-arrow-up-white";
					$categorySelection = "in";
					$categoryHeight = 'style="height:auto;"';
					$styleCat = "style='background-color: rgb(66, 119, 0);'";					
				} else {
					$cataSelection = "";
					$catSelection = "single-nav-wrap";
					$accordionSelection = "";
					$categorySelection = "";
					$categoryHeight = "";
					$styleCat = "";
				}
				
			?>
<!-- 				<dd><span <?=$on?> ><?=$catName?></span> -->
				
			        <div id="cat<?=$catid?>" class="<?=$catSelection?>" <?=$styleCat?>>
                    <a href="<?=$root?>products/<?=$catNameURL?>" class="link-left <?=$cataSelection?>"><?=$catName?></a>
                    
                    
			      	<a class="accordion-toggle <?=$accordionSelection?> link-arrow" data-toggle="collapse" data-parent="#accordion2" href="#category<?=$catid?>" id="arrow" rel="<?=$catid?>"></a>

		        </div>
			        <div id="category<?=$catid?>" class="accordion-body <?=$categorySelection?> collapse" <?=$categoryHeight?>>

		        	<ul class="unstyled collapse">
					<?php
					/*if ($catNameURL=='medical_products/default.html') {
						?>
						<li><a href="<?=$root?>products/<?=$catNameURL?>"><?=$catName?></a></li>
						<?php
					} else {*/
						$subcategory = Subcategory::displayAll($catid);
						foreach ($subcategory as $subcatR) {
							$subcategoryName = $subcatR->fldSubcategoryName;
							$subcatNameURL 	= $subcatR->fldSubcategoryURL;
							?>
							<li><a href="<?=$root?>products/<?=$subcatNameURL?>"><?=$subcategoryName?></a></li>
							<?php
						}
						?>
							<?php
					//}
					?>
		            </ul>
		        </div> 
			<?php
			}
			$on = "";
			$ii++;
		}
		?>
	</div>
</dl>

<!-- 
	<div class="nav-wrapper">
        <div id="cat1" class="single-nav-wrap">
	        <a href="#" class="link-left">Cat1</a>
	        <a class="accordion-toggle link-arrow" data-toggle="collapse" data-parent="#accordion2" href="#demo" id="arrow"></a>
        </div>
        
        <div id="demo" class="accordion-body collapse in">
        	<ul class="unstyled collapse">
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
            </ul>
        </div> 
            
     	<div id="cat2" class="single-nav-wrap">
	        <a href="#" class="link-left">Cat1</a>
	        <a class="accordion-toggle link-arrow" data-toggle="collapse" data-parent="#accordion2" href="#demo2" id="arrow"></a>
        </div>
        
        <div id="demo2" class="accordion-body collapse">
        	<ul class="unstyled collapse">
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
	            <li><a href="#">test</a></li>
            </ul>
        </div>
	</div>
 -->
</aside>


<script type="text/javascript">

//$(function() {
 // $('.link-left , .link-arrow , .single-nav-wrap').hover(function() {
  //  $('.single-hover').css('display', 'block');
//	$('.link-left').css('color', '#ffffff');
//	$('.link-arrow').css('background-image', 'url(<?=$root?>assets/images/icon-green-arrow-bg-green.jpg');
  //}, function() {
    // on mouseout, reset the background colour
 //   $('.single-hover').css('display', 'none');	
//	$('.link-left').css('color', '#555555');
//	$('.link-arrow').css('background-image', 'url(<?=$root?>assets/images/icon-green-arrow.jpg)');
 // });
//});

$(document).ready( function() {
  $('.single-nav-wrap').hover(function() {
   		divid = $(this).attr("id");  
		//alert(divid);
		$("#"+divid).css('background-color' , '#427700');
		$("#"+divid + " a.link-left").addClass("link-white");
		$("#"+divid + " a#arrow").removeClass("link-arrow").addClass("test-arrow");
		},
		function() {
			divid = $(this).attr("id");  
			
			$(".single-nav-wrap").css('background-color' , '');							

			$(".link-left").removeClass("link-white");		

			//$(".link-left").removeAttr('style');
			$("#"+divid + " a#arrow").removeClass("test-arrow").addClass("link-arrow");

			<? if($categoryID!="") { ?>
				
				if ($('#cat<?=$categoryID?>').hasClass('link-white')) {
					
					$("#cat<?=$categoryID?> a").removeClass("link-white");
				} else {		
					//alert($("#cat<?=$categoryID?>").attr('style'));
					//var attr = $("#cat<?=$categoryID?>").attr('style');

   					 if($("#cat<?=$categoryID?>").is("style") || $("#cat<?=$categoryID?>").hasClass('single-hover')) {
						
						$("#cat<?=$categoryID?> a.link-left").addClass("link-white");					

					} else {
						//alert("ok");
						$("#cat<?=$categoryID?> a.link-left").removeClass("link-white");				
					}
				}
				
			<? } ?>
			
			
		
		}); 		
  });
  
 	
(function($){ 
   $(".accordion-toggle").click(function() {     		
		if($("#"+divid + " a").attr("class")=="link-white") {
			$("#"+divid + " a.link-white").removeClass("link-white").addClass("link-left");
			$("#"+divid + " a#arrow").removeClass("link-arrow-up-white").addClass("link-arrow");
			$("#"+divid + ".single-hover").removeClass("single-hover").addClass("single-nav-wrap");
			$("#"+divid).removeAttr("style"); 
			//$(".single-nav-wrap").css('background-color' , '');
			//alert($("#"+divid).attr("class"));			
		} else {
	   		$("#"+divid + " a.link-left").removeClass("link-left").addClass("link-white");
			$("#"+divid + " a#arrow").removeClass("test-arrow").addClass("link-arrow-up-white");
			$("#"+divid + ".single-nav-wrap").removeClass("single-nav-wrap").addClass("single-hover");

		}


		
		
		  
		  
	  // $(this).toggleClass("link-arrow-up-white");
	  // $("#"+divid).toggleClass("single-hover");
	   
	   
	  // $("#"+$(this).attr("id")+ " a.link-left").toggleClass("link-white");
	  // $("#"+divid + " a.link-left").css("color" , "#ffffff");
	  
	 	toggleID = $(this).attr('rel');		    
		$(".accordion-toggle").each(function(){	
					
			if($(this).attr('rel') != toggleID) {
				
				if($(this).attr('rel')!= "arrow" ) { 
					$(this).addClass("collapsed");				
					categoryID = "#category"+$(this).attr('rel');				
					$(categoryID).removeClass("in");
					$(categoryID).height(0);
					$("#cat"+$(this).attr('rel') + " a.link-white").removeClass("link-white").addClass("link-left");
					$("#cat"+$(this).attr('rel') + " a#arrow").removeClass("link-arrow-up-white").addClass("link-arrow");
					$("#cat"+$(this).attr('rel') + ".single-hover").removeClass("single-hover").addClass("single-nav-wrap");
					$("#cat"+$(this).attr('rel')).removeAttr("style"); 
					
				}
			} else {
				$(this).removeClass("collapsed");	
		
			}	
		}); 

	  	    <? if($categoryID!="") { ?>				
	
					//$("#cat<?=$categoryID?> a").removeClass("link-white");
				
			<? } ?>
   });
   
        	
   
   
   
 })(jQuery); 
	    
</script>