<?php

$message  = '<div align="center" style="width:650px;margin:auto;padding:10px 0;">
            <table border="0" cellpadding="2" cellspacing="2" width="650" style="background:rgba(0,0,0,0.10);">
              <tr>	
                <th width="60%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Product Name </th>
                <th width="10%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Amount </th>
                <th widht="10%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> QTY </th>
                <th width="20%" style="background:#427700;font:600 13px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> Total </th>
              </tr>';

//display the shopping cart
$date = date('Y-m-d');

$cart = Cart::displayCartByOrderNo($order_code);
foreach($cart as $carts) { 
	$products = Products::findProducts($carts->fldCartProductID);

	$image = $root."uploads/product_image/".$carts->fldCartProductID."/_75_".$products->fldProductsImage;

	$subtotal = $carts->fldCartProductPrice * $carts->fldCartQuantity;
	$countitem = $countitem + 1;

	$total = $total + $subtotal; 

	$product_price_text = ($carts->fldCartProductPriceText!='')? '<br>** '.$carts->fldCartProductPriceText.' **': '';
    if ($_SESSION['client_type']==2) { // Corporate
    	$corporate_price_text = "<br><small>** ".$products->fldProductsPrice2Text." **</small>";
    }

	// Product Option + Variant
	$variant_id = $carts->fldCartProductVariant;

	if (!empty($variant_id)) {
		$variant = ProductVariant::findVariant($variant_id);
		$variant_name = $variant->product_variant;
		$option_id = $variant->product_option_id;
		$option = ProductVariant::findOption($option_id);
		// $option_name = $option->product_option;
		$option_name = ($option->product_option=="Product Options")? "": $option->product_option." | ";

	  	$product_variant = "<br><small> ".$option_name.$variant_name."</small>";
	}

	// Products
	$message .= '<tr>
	                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">'.$carts->fldCartProductName.$product_price_text.$product_variant.$corporate_price_text.'<br>';

    if (strtolower($option->product_option)!='accessories') {
		$message .= '<img src="'.$image.'" alt="'.$carts->fldCartProductName.'" style="display:inline-block;margin-right:10px;vertical-align:middle;box-shadow:1px 1px 3px 0 rgba(0,0,0,0.75);">';
    }

	$message .= '</td>
	                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">$ '.number_format($carts->fldCartProductPrice,2).'</td>
	                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;">'.$carts->fldCartQuantity.'</td>
	                <td style="font:400 13px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:10px;"> $ '.number_format($subtotal,2).'</td>
	              </tr>';
	}

              
$message .= '<tr><td colspan="4"><hr></td></tr>';

$message .= '<tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  Sub-Total:
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  $ '.number_format($total,2).'
                </td>
              </tr>';
              // <!-- SUB-TOTAL -->

            // TAX
			$tax = Tax::findTaxByOrderCode($order_code);
			$taxAmount 	= $tax->fldTaxValue;
$message .= '<tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  TAX:
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  $ '.number_format($taxAmount,2).'
                </td>
              </tr>';
              // <!-- TAX -->

            // COUPON
			$coup = Cart::getCartXCoupon($order_code);
			$couponAmount 	= $coup->fldCXCCouponAmount;
			// $couponCode 	= $coup->fldCXCCoupon;
			$couponCode 	= ($couponAmount > 0)? $coup->fldCXCCoupon : '';
$message .= '<tr>
                <td colspan="3" style="font:600 13px sans-serif;color:#555;text-align:right;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  COUPON ('.$couponCode.') :
                </td>
                <td style="font:400 12px sans-serif;color:#555;text-align:left;text-transform:uppercase;text-shadow:none;padding:5px 10px;">
                  $ '.number_format($couponAmount,2).'
                </td>
              </tr>';
              // <!-- DISCOUNT CODE -->

			$grandtotal = ($total + $taxAmount - $couponAmount);
$message .= '<tr>
                <td colspan="3" style="background:#427700;font:600 16px sans-serif;color:#FFF;text-align:right;text-transform:uppercase;text-shadow:none;padding:15px 10px;">
                  GRAND TOTAL:
                </td>
                <td style="background:#427700;font:400 14px sans-serif;color:#FFF;text-align:left;text-transform:uppercase;text-shadow:none;padding:15px 10px;">
                  $ '.number_format($grandtotal,2).'
                </td>
              </tr>';
              // <!-- GRAND TOTAL -->
              
$message .= '</table>
          </div>';
          // <!-- ORDERS PANEL -->';

		
		require("includes/class.phpmailer.php");
	    $mail = new PHPMailer();
	    
		// Send Order Receipt to Super Admin
		$name = 'ICP America Administrator';
		$from = $clients->fldBillingEmail;
		$mail->From = $from;
		$mail->FromName = $clients->fldBillingFirstName . ' ' . $clients->fldBillingLastname;

		/*
		$condSuperAdmin = "fldAdministratorLevel ='1' ";
		$admin = Administrator::findAllAdministratorByCondition($condSuperAdmin);
		foreach ($admin as $adm) {
		  $mail->AddAddress($adm->fldAdministratorEmail); 
		}
		*/
	  	// $mail->AddAddress('apuglisi@icpamerica.com');
	  	// $mail->AddAddress('ktinsley@icpamerica.com');
	  	$mail->AddAddress('sales@icpamerica.com'); 
		$mail->AddBCC('test1@dogandrooster.net');

		$mail->IsHTML(true); // set email format to HTML
		$all_html = implode('',file('includes/orders.php'));
		
		//FOR BILLING
		$name = $clients->fldBillingFirstName . ' ' . $clients->fldBillingLastname;
		$address = $clients->fldBillingAddress . ' ' . $clients->fldBillingCity . ' ' . $clients->fldBillingState . ' ' . $clients->fldBillingCountry . ' ' . $clients->fldBillingZip;
		$email = $clients->fldBillingEmail;
		$contact = $clients->fldBillingPhoneNo;
		
		//FOR SHIPPING		
		$shipping_name = $shipping->fldShippingFirstName . ' ' . $shipping->fldShippingLastname;
		$shipping_address = $shipping->fldShippingAddress . ' ' . $shipping->fldShippingCity . ' ' . $shipping->fldShippingState . ' ' . $shipping->fldShippingCountry . ' ' . $shipping->fldShippingZip;
		$shipping_email = $shipping->fldShippingEmail;
		$shipping_contact = $shipping->fldShippingPhoneNo;
		
		$all_html = str_replace("%%order_no%%", $order_code, $all_html);
		$all_html = str_replace("%%order_date%%", date('F d, Y'), $all_html);
		$all_html = str_replace("%%name%%", $name, $all_html);
		$all_html = str_replace("%%address%%", $address, $all_html);
		$all_html = str_replace("%%email%%", $email, $all_html);
		$all_html = str_replace("%%contact%%", $contact, $all_html);
		$all_html = str_replace("%%shipping_name%%", $shipping_name, $all_html);
		$all_html = str_replace("%%shipping_address%%", $shipping_address, $all_html);
		$all_html = str_replace("%%shipping_email%%", $shipping_email, $all_html);
		$all_html = str_replace("%%shipping_contact%%", $shipping_contact, $all_html);
		$all_html = str_replace("%%orders%%", $message, $all_html);
											
		//name of client						 																														
		$mail->Subject = "New Order - ICP America";
		//$alt_body = $_POST['body'];
		$mail->Body    = $all_html;
		//$mail->AltBody = $alt_body;
		if($mail->Send()){
			$mail->ClearAddresses();
		}
		
		
		// Send Order Receipt to Client
		// $name 	= $fldClientFirstName.' '.$fldClientLastname;
		$name 	= $clients->fldBillingFirstName.' '.$clients->fldBillingLastname;
		$to  	= $clients->fldBillingEmail;
		$from 	='sales@icpamerica.com';
		$mail->From = $from;
		$mail->FromName ='sales@icpamerica.com';
		$mail->AddAddress($to);
		$mail->AddBCC('test1@dogandrooster.net');

		$mail->IsHTML(true); // set email format to HTML
		$all_html = implode('',file('includes/orders.php'));
		
		$all_html = str_replace("%%order_no%%", $order_code, $all_html);
		$all_html = str_replace("%%order_date%%", date('F d, Y'), $all_html);
		$all_html = str_replace("%%name%%", $name, $all_html);
		$all_html = str_replace("%%address%%", $address, $all_html);
		$all_html = str_replace("%%email%%", $email, $all_html);
		$all_html = str_replace("%%contact%%", $contact, $all_html);
		$all_html = str_replace("%%shipping_name%%", $shipping_name, $all_html);
		$all_html = str_replace("%%shipping_address%%", $shipping_address, $all_html);
		$all_html = str_replace("%%shipping_email%%", $shipping_email, $all_html);
		$all_html = str_replace("%%shipping_contact%%", $shipping_contact, $all_html);
		$all_html = str_replace("%%orders%%", $message, $all_html);

		//name of client						 																														
		$mail->Subject = "Your Order - ICP America";
		//$alt_body = $_POST['body'];
		$mail->Body    = $all_html;
		//$mail->AltBody = $alt_body;
		if($mail->Send()){
			$mail->ClearAddresses();
		}
?>