<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li><a href="<?=$root?>products/default.html">Products</a> <span class="divider">/</span></li>
<?php /*	    <li><a href="<?=$root?>products/<?=$category_url?>"><?=$category_name?></a> <span class="divider">/</span></li>
	    <?php if ($subcategory_name) { ?><li><a href="<?=$root?>products/<?=$subcategory_url?>"><?=$subcategory_name?></a> <span class="divider">/</span></li><?php } ?>
	    <?php if ($subcategory2_name) { ?><li class="active"><a href="<?=$root?>products/<?=$subcategory2_url?>"><?=$subcategory2_name?></a></li><?php } ?> */ ?>
		<? $title = $category_name; ?>	
		<li><a href="<?=$root?>products/<?=$category_url?>"><?=$bread_cat1?></a> <span class="divider">/</span></li>
		<?php if ($bread_cat2) { $title = $subcategory_name;?> <li><a href="<?=$root?>products/<?=$subcategory_url?>"><?=$bread_cat2?></a> <span class="divider">/</span></li> <?php } ?>
		<?php if ($bread_cat3) { $title = $subcategory2_name;?> <li><a href="<?=$root?>products/<?=$subcategory2_url?>"><?=$bread_cat3?></a> <span class="divider">/</span></li> <?php } ?>
    </ul>

	<?php
    foreach ($subcat as $subcatR) {
		$subcatID 		= $subcatR->fldSubcategoryID;
		$subcatName 	= $subcatR->fldSubcategoryName;
		$subcatImage 	= $subcatR->fldSubcategoryImage;
		$subcatURL 		= $subcatR->fldSubcategoryURL;
        ?>

		<h1><?=$title?></h1>

		<ul class="unstyled clearfix product-listing">
		<?php
	    $condition = "AND fldProductsSubcategory2ID = '".$subcatID."' AND fldProductsURL LIKE '".substr($prodURL, 0, -12)."%'";
	    
	    // For Medical Products OVERRIDE
	    if ($prodURL == 'medical_products/default.html') {
	    	$condition = "AND fldProductsSubcategoryID = '".$subcatID."' AND fldProductsURL LIKE '".substr($prodURL, 0, -12)."%'";
	    }

	    $prod = Products::displayAllByCondition($categoryID, $condition);

	    $countProd = Products::countAllByCondition($categoryID, $condition);

		if ($countProd >= 1) {
			foreach ($prod as $prodR) {
				$productID 			= $prodR->fldProductsId;
				$productName 		= $prodR->fldProductsName;
				$productImage 		= $prodR->fldProductsImage;
				$productCode 		= $prodR->fldProductsCode;
				$productURL 		= $prodR->fldProductsURL;
				$productPDF 		= $prodR->fldProductsPDF;
				$productOverview 	= $prodR->fldProductsOverview;
				$productInfo 		= $prodR->fldProductsInformation;
				$productDesc 		= $prodR->fldProductsDescription;

				// Tiered Pricing
				if ($_SESSION['client_type'] == 1) { $productPrice = $prodR->fldProductsPrice1; $productPriceText = $prodR->fldProductsPrice1Text; }
				elseif ($_SESSION['client_type'] == 2) { $productPrice = $prodR->fldProductsPrice2; $productPriceText = $prodR->fldProductsPrice2Text; }
				elseif ($_SESSION['client_type'] == 3) { $productPrice = $prodR->fldProductsPrice3; $productPriceText = $prodR->fldProductsPrice3Text; }
				?>
				<li>
	    		<form action="<?=$root?>shopping-cart.html" method='POST'>
						<table width="100%">
							<tbody>
								
								<tr>
								<td class="product-img">
									<a href="<?=$root?>products/<?=$productURL?>">
										<img src="<?=$root?>uploads/product_image/<?=$productID?>/_190_<?=$productImage?>" alt="<?=$productName?>">
									</a>
								</td>
								<td class="product-desc">
									<h4><a href="<?=$root?>products/<?=$productURL?>"><?=$productCode?></a></h4>
									<?=$productOverview?>
								</td>
								<td class="product-action">
									<? if($_SESSION['logged_in']!=1) { ?>
										<h4>sign up</h4>
										<a href="<?=$root?>login.html" class="login">login</a>
										<a href="<?=$root?>products/<?=$productURL?>" class="view">view</a>
									<? } else { ?>
				    					<?php
				    					// Check for Text before Price
				    					if ($productPriceText!='') { // Show Price Text
				    						?>
											<h4><?=$productPriceText?></h4>
				    						<?php
				    						$productPrice = "0.00";
				    					} else { // Show Price
				    						?>
											<h4>$ <?=$productPrice?></h4>
				    						<?php
				    					}
				    					?>

										<input type='hidden' name='product_id' value='<?=$productID?>'>
										<input type='hidden' name='product_name' value='<?=$productName?>'>
										<input type='hidden' name='product_price' value='<?=$productPrice?>'>
										<input type='hidden' name='product_price_text' value='<?=$productPriceText?>'>
										<? /* <button type="submit" class="add2cart" name="add2cart">add to cart</button> */ ?>
										<a href="<?=$root?>products/<?=$productURL?>" class="view">view</a>
									<? } ?>
								</td>
								</tr>
						
							</tbody>
						</table>
					</form>
				</li>
				<?php
			}

		} else {
			?>
<!-- 			<li>No Product/s found.</li> -->
			No Product/s found.
			<?php
		}
		?>

		</ul>

        <?php
    }
	?>



	</section>
	<!-- End of Content Panel -->

</article>