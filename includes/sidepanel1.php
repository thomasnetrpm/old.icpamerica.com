<aside>

	

	<dl id="category" class="side-framebox clearfix">
		<dt>Category</dt>
		<?php
		$category = Category::displayAllCategory();
		foreach ($category as $catR) {
			$categoryName 	= $catR->fldCategoryName;
			$categoryID 	= $catR->fldCategoryID;
			$categoryNameURL= str_replace(' ', '_', strtolower($categoryName));
			?>
			<dd><span><?=$categoryName?></span>
			<ul class="unstyled">
			<?php
			$subcategory = Subcategory::displayAll($categoryID);
			foreach ($subcategory as $subcatR) {
				$subcategoryName = $subcatR->fldSubcategoryName;
				$subcatNameURL 	= str_replace('/', '__', str_replace(' ', '_', strtolower($subcategoryName)));
				?>
				<li><a href="<?=$root?>products/<?=$categoryNameURL?>/<?=$subcatNameURL?>/default.html"><?=$subcategoryName?></a></li>
				<?php
			}
			?>
			</ul>
		</dd>
			<?php
		}
		?>
	</dl>

</aside>