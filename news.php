<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

include 'classes/NewsCategory.php';
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li class="active"><a href="<?=$root?>">home</a> <span class="divider">/</span></li>
	    <li>news</li>
    </ul>

		<h1>News</h1>
		<?php
		$category = NewsCategory::findAll($pg[0]);
		foreach ($category as $cat) {
			$categoryID = $cat->nc_id;
			$categoryName = $cat->nc_name;
			?>
			<ul class="unstyled" style="border-bottom:solid 1px #CCC;">
				<li class="title-news"><?=$categoryName?></li>
				<li>
					<?php
					$news = News::displayAllByCategory($categoryID);
                   
					foreach ($news as $nw) {
						$newsID 		= $nw->fldNewsID;
						$newsTitle 		= $nw->fldNewsTitle;
						$newsURL 		= $nw->fldNewsURL;
						$newsContent	= $nw->fldNewsDescription;
						$newsPDF 		= $nw->fldNewsPDF;
                        $target_blank = "";
						if ($newsPDF!="") {
							$link = $root."uploads/news_pdf/".$newsPDF;
                             $target_blank = 'target="_blank"';
						} else {
							$link = '#';
						}
						?>
						<dl class="dl-horizontal">
    						<dt><div class="dl-pdf-date"><?=$newsTitle?></div>
                            	<?php if ($newsPDF!="") { ?>
                                	<div><a href="<?=$link?>" target="_blank" class="dl-pdf"></a></div>
                                <?php } ?>
                            </dt>
							<dd><?=$newsContent?></dd>
						</dl>
						<!-- end -->
						<?php
					}
					?>
				</li>
			</ul>
			<?php
		}
		?>

	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<style>
	.title-news { font-weight: 600; font-size: 16px; }
	.dl-horizontal { margin: 10px 0; }
</style>
<? endblock(); ?>

<? startblock('script') ?>
<? endblock(); ?>