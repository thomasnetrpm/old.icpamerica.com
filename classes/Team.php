<?

class Team{
     
    function addTeam($team,$image,$image2){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($team->description);		
		$name = addslashes($team->name);
		$position = addslashes($team->position);

		
        $query="INSERT INTO ".$table_prefix."_tblTeam SET ".           			
			"fldTeamCategory='$team->category_id',".
			"fldTeamName='$name',".
			"fldTeamPosition='$position',".
			"fldTeamEmailAddress='$team->email',".
			// "fldTeamImage='$image',".
			"fldTeamImageOverview='$image',".
			"fldTeamImageDetails='$image2',".
            "fldTeamDescription='$content'";
		$adb->query($query);
        return true;
    }
	
	function updateTeam($team,$image,$image2) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($team->description);		
		$name = addslashes($team->name);
		$position = addslashes($team->position);

		
		$query  = "UPDATE ".$table_prefix."_tblTeam SET ".
	    	"fldTeamCategory='$team->category_id', ".
			"fldTeamName='$name', ".
			"fldTeamPosition='$position', ".
			"fldTeamEmailAddress='$team->email', ";
		if($image) {
			$query .= "fldTeamImageOverview='$image', ";
		}
		if($image2) {
			$query .= "fldTeamImageDetails='$image2', ";
		}
        $query .= "fldTeamDescription='$content' ".
	        "WHERE fldTeamId=$team->Id ";

/*		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblTeam SET ".
        	"fldTeamCategory='$team->category_id',".	
			"fldTeamName='$name',".		
			"fldTeamPosition='$position',".	
			"fldTeamEmailAddress='$team->email',".
            "fldTeamDescription='$content'".
            " WHERE fldTeamId=$team->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblTeam SET ".
        	"fldTeamCategory='$team->category_id',".	
			"fldTeamName='$name',".	
			"fldTeamPosition='$position',".	
			"fldTeamEmailAddress='$team->email',".
			// "fldTeamImage='$image',".
			"fldTeamImageOverview='$image',".
            "fldTeamDescription='$content'".
            " WHERE fldTeamId=$team->Id";
		}
*/
	    $adb->query($query);
		
		
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($category_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamCategory='$category_id'";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllTeam($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamId!='$id'";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countTeam() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}
	
	function findTeam($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findTeamfirst(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblTeam LIMIT 1 ORDER BY Id";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteTeam($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblTeam WHERE fldTeamId='$id'";
        $adb->query($query);
        return true;
    }
    
	function displayAllByCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam ORDER BY fldTeamCategory, fldTeamCategoryOrder ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function displayAllByCategoryID($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamCategory='$id' ORDER BY fldTeamCategoryOrder ASC ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function countAllByCategoryID($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam WHERE fldTeamCategory='$id' ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function updateTeamCategoryOrder($teamID, $order) {
		global $adb;
		global $table_prefix;
		
		$query  = "UPDATE ".$table_prefix."_tblTeam SET ".
        	"fldTeamCategoryOrder='$order' ".
	        "WHERE fldTeamID=$teamID ";
	    $adb->query($query);
        return true;
	}

	function displayAllByOrder($orderby) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblTeam ORDER BY $orderby ";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function updateTeamOverviewOrder($teamID, $order) {
		global $adb;
		global $table_prefix;
		
		$query  = "UPDATE ".$table_prefix."_tblTeam SET ".
        	"fldTeamOverviewOrder='$order' ".
	        "WHERE fldTeamID=$teamID ";
	    $adb->query($query);
        return true;
	}


}
?>
