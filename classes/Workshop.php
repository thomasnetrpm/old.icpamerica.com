<?
   
class Workshop{
   
    function addWorkshop($events){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($events->description);		
		$name = addslashes($events->name);
		$date = date('Y-m-d');
        $query="INSERT INTO ".$table_prefix."_tblWorkshop SET ".
            "fldWorkshopName='$name',".
			"fldWorkshopDescription='$description',".	
            "fldWorkshopDate='$date'";
        $adb->query($query);
        return true;
    }
	
	function updateWorkshop($events) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($events->description);
		$name = addslashes($events->name);
		
		
			$query="UPDATE ".$table_prefix."_tblWorkshop SET ".
				"fldWorkshopName='$name',".
				"fldWorkshopDescription='$description',".	
				"fldWorkshopDate='$events->date'".
           		 " WHERE fldWorkshopID=$events->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshop";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshop ORDER BY fldWorkshopDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countWorkshop() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshop";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findWorkshop($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblWorkshop WHERE fldWorkshopID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findLatestWorkshop(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblWorkshop ORDER BY fldWorkshopDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteWorkshop($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblWorkshop WHERE fldWorkshopID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
