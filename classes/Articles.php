<?
   
class Articles{
   
    function addArticles($articles){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($articles->description);
		$name = addslashes($articles->name);

        $query="INSERT INTO ".$table_prefix."_tblArticles SET ".
            "fldArticlesName='$name',".
			"fldArticlesDescription='$description',".	
            "fldArticlesDate='$articles->date'";
        $adb->query($query);
        return true;
    }
	
	function updateArticles($articles) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($articles->description);
		$name = addslashes($articles->name);
		
			$query="UPDATE ".$table_prefix."_tblArticles SET ".
				"fldArticlesName='$name',".
				"fldArticlesDescription='$description',".	
				"fldArticlesDate='$articles->date'".
           		 " WHERE fldArticlesID=$articles->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblArticles ORDER BY fldArticlesDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblArticles ORDER BY fldArticlesDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllArticles($start,$end) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblArticles ORDER BY fldArticlesDate DESC LIMIT $start, $end";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countArticles() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblArticles";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findArticles($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblArticles WHERE fldArticlesID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findLatestArticles(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblArticles ORDER BY fldArticlesDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteArticles($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblArticles WHERE fldArticlesID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
