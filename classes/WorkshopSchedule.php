<?
   
class WorkshopSchedule{
   
    function addWorkshopSchedule($schedule){
        global $adb;
		global $table_prefix;
		
		$title = addslashes($schedule->title);
		$date = addslashes($schedule->dateSchedule);
		$time = addslashes($schedule->timeSchedule);

        $query="INSERT INTO ".$table_prefix."_tblWorkshopSchedule SET ".
            "fldWorkshopID='$schedule->event_id',".
			"fldWorkshopScheduleTitle='$title',".
			"fldWorkshopScheduleDate='$date',".	
			"fldWorkshopScheduleTime='$time'";
        $adb->query($query);
        return true;
    }
	
	function updateWorkshopSchedule($schedule) {
		 global $adb;
		 global $table_prefix;
		 
		$title = addslashes($schedule->title);
		$date = addslashes($schedule->dateSchedule);
		$time = addslashes($schedule->timeSchedule);
		
			$query="UPDATE ".$table_prefix."_tblWorkshopSchedule SET ".
				"fldWorkshopScheduleTitle='$title',".
				"fldWorkshopScheduleDate='$date',".	
				"fldWorkshopScheduleTime='$time'".
           		 " WHERE fldWorkshopScheduleID=$schedule->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg,$id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule WHERE fldWorkshopID='$id'";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule ORDER BY fldWorkshopScheduleDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function displayAllByEvents($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule WHERE fldWorkshopID='$id' ORDER BY fldWorkshopScheduleID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countWorkshopSchedule($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule WHERE fldWorkshopID='$id'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findWorkshopSchedule($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule WHERE fldWorkshopScheduleID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findLatestWorkshopSchedule(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblWorkshopSchedule ORDER BY fldWorkshopScheduleDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteWorkshopSchedule($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblWorkshopSchedule WHERE fldWorkshopScheduleID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
