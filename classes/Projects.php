<?

class Projects{
     
    function addProjects($projects,$image){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($projects->name);
		$description = addslashes($projects->description);
		$location = addslashes($projects->location);
		$meta_title = addslashes($projects->meta_title);
		$meta_keywords = addslashes($projects->meta_keywords);
		$meta_description = addslashes($projects->meta_description);
		
        $query="INSERT INTO ".$table_prefix."_tblProjects SET ".           			
			"fldProjectsCategoryId='$projects->category_id',". 	
			"fldProjectsImage='$image',". 
			"fldProjectsDescription='$description',". 
			"fldProjectsLocation='$location',". 
			"fldProjectsPosition='$projects->positions',". 
			"fldProjectsMetaTitle='$projects->meta_keywords',". 
			"fldProjectsMetaKeywords='$projects->meta_keywords',". 
			"fldProjectsMetaDescription='$projects->meta_description',". 
			"fldProjectsName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateProjects($projects,$image) {
		 global $adb;
		 global $table_prefix;
		 

		$name = addslashes($projects->name);
		$description = addslashes($projects->description);
		$location = addslashes($projects->location);
		$meta_title = addslashes($projects->meta_title);
		$meta_keywords = addslashes($projects->meta_keywords);
		$meta_description = addslashes($projects->meta_description);
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblProjects SET ".
           "fldProjectsName='$name',".
		   "fldProjectsLocation='$location',". 
		   "fldProjectsPosition='$projects->positions',". 
		   "fldProjectsMetaTitle='$meta_title',". 
			"fldProjectsMetaKeywords='$meta_keywords',". 
			"fldProjectsMetaDescription='$meta_description',".
		   "fldProjectsDescription='$description'". 
		    " WHERE fldProjectsID=$projects->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblProjects SET ".
			"fldProjectsImage='$image',". 	
			"fldProjectsLocation='$location',". 
			"fldProjectsDescription='$description',". 
			"fldProjectsPosition='$projects->positions',". 
			"fldProjectsMetaTitle='$meta_title',". 
			"fldProjectsMetaKeywords='$meta_keywords',". 
			"fldProjectsMetaDescription='$meta_description',".
           "fldProjectsName='$name'". 			
		    " WHERE fldProjectsID=$projects->Id";
			
		}
           
		
	    $adb->query($query);
        return true;
	}
	
	function updateProjectsPositions($positions,$project_id) {
		 global $adb;
		 global $table_prefix;
		 
			$query="UPDATE ".$table_prefix."_tblProjects SET ".
			"fldProjectsPosition='$positions'". 	
		    " WHERE fldProjectsID='$project_id'";
		$adb->query($query);
        return true;
	}
    
	function findAll($pg,$id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjects WHERE fldProjectsCategoryId='$id' ORDER BY fldProjectsPosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjects WHERE fldProjectsCategoryId='$id' ORDER BY fldProjectsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllProjects() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjects ORDER BY fldProjectsID DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countProjects($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjects WHERE fldProjectsCategoryId='$id'";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findProjects($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProjects WHERE fldProjectsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteProjects($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProjects WHERE fldProjectsID='$id'";
        $adb->query($query);
        return true;
    }
    
	function getProjectCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT DISTINCT(fldProjectsCategoryId) FROM ".$table_prefix."_tblProjects ORDER BY fldProjectsCategoryId ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
   
	function getProjectByCategory($category_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjects WHERE fldProjectsCategoryId='$category_id' ORDER BY fldProjectsID ASC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
}
?>
