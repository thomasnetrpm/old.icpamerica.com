<?

class HomePage{
   
    function addHomePage($pages,$banner){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($pages->content);
		$registration = addslashes($pages->registration);	
		$schedule = addslashes($pages->schedule);	
		$course = addslashes($pages->course);	
		
		$date = date('Y-m-d');
		
		
		
			
        $query="INSERT INTO ".$table_prefix."_tblHomePage SET ".
            "fldHomePageRegistration='$registration',".	
			"fldHomePageSchedule='$schedule',".	
			"fldHomePageCourse='$course',".	
			"fldHomePageDateModified='$date',".
			"fldHomePageBanner='$banner',".
            "fldHomePageDescription='$content'";
        $adb->query($query);
        return true;
    }
	
	function updateHomePage($pages,$banner) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($pages->content);
		$registration = addslashes($pages->registration);	
		$schedule = addslashes($pages->schedule);	
		$course = addslashes($pages->course);	
		
		if($banner == "") { 
			$query="UPDATE ".$table_prefix."_tblHomePage SET ".
				"fldHomePageRegistration='$registration',".	
				"fldHomePageSchedule='$schedule',".	
				"fldHomePageCourse='$course',".	
				"fldHomePageDescription='$content'".
           		 " WHERE fldHomePageID=$pages->Id";
		} else {
				$query="UPDATE ".$table_prefix."_tblHomePage SET ".
				 "fldHomePageRegistration='$registration',".	
				 "fldHomePageSchedule='$schedule',".	
				 "fldHomePageCourse='$course',".	
				 "fldHomePageBanner='$banner',".
            	 "fldHomePageDescription='$content'".
           		 " WHERE fldHomePageID=$pages->Id";
		}
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePage";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePage ORDER BY fldHomePageName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countHomePage() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblHomePage";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findHomePage($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblHomePage WHERE fldHomePageID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteHomePage($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblHomePage WHERE fldHomePageID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
