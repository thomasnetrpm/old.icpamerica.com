<?

class PastGuest{
     
    function addPastGuest($guest){
        global $adb;
		global $table_prefix;
		
		
		$name = addslashes($guest->name);
		$content = addslashes($guest->content);
		
		
        $query="INSERT INTO ".$table_prefix."_tblPastGuest SET ".           							
			"fldPastGuestName='$name',".		
			"fldPastGuestContent='$content',".
			"fldPastGuestDate='$guest->date'";            
        $adb->query($query);
        return true;
    }
	
	function updatePastGuest($guest) {
		 global $adb;
		 global $table_prefix;
		 
		
		$name = addslashes($guest->name);
		$content = addslashes($guest->content);
		
		
			$query="UPDATE ".$table_prefix."_tblPastGuest SET ".
         	"fldPastGuestName='$name',".
			"fldPastGuestContent='$content',".
			"fldPastGuestDate='$guest->date'".
        
            " WHERE fldPastGuestId=$guest->Id";
		
		
	    $adb->query($query);
		
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest ORDER BY fldPastGuestDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest ORDER BY fldPastGuestDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBydate($dateFrom,$dateTo) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest WHERE fldPastGuestDate BETWEEN '$dateFrom' AND '$dateTo' ORDER BY fldPastGuestDate DESC";		
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayFirst4() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest ORDER BY fldPastGuestDate DESC LIMIT 4";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllPastGuest() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest ORDER BY fldPastGuestName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countPastGuest() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPastGuest";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findPastGuestByPosition($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPastGuest WHERE fldPastGuestCategory='$id' ORDER BY fldPastGuestDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findPastGuest($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPastGuest WHERE fldPastGuestId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deletePastGuest($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblPastGuest WHERE fldPastGuestId='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
