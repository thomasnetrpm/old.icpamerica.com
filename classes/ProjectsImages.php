<?

class ProjectsImages{
     
    function addProjectsImages($projects,$image){
        global $adb;
		global $table_prefix;
		

        $query="INSERT INTO ".$table_prefix."_tblProjectsImages SET ".           			
			"fldProjectsImagesProjectsId='$projects->project_id',". 	
			"fldProjectsImagesImage='$image'"; 
        $adb->query($query);
        return true;
    }
	
	function updateProjectsImages($projects,$image) {
		 global $adb;
		 global $table_prefix;
		 
		if($image != "") {
			$query="UPDATE ".$table_prefix."_tblProjectsImages SET ".
			"fldProjectsImagesImage='$image',". 	
			"fldProjectsImagesPosition='$projects->positions'". 	
		    " WHERE fldProjectsImagesID=$projects->Id";
			
		}
           
		
	    $adb->query($query);
        return true;
	}
	
	function updateProjectsImagesPosition($position,$image_id) {
		 global $adb;
		 global $table_prefix;
		 
			$query="UPDATE ".$table_prefix."_tblProjectsImages SET ".
			"fldProjectsImagesPosition='$position'". 	
		    " WHERE fldProjectsImagesID='$image_id'";
			
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjectsImages order by fldProjectsImagesPosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($projectid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjectsImages WHERE fldProjectsImagesProjectsId='$projectid' ORDER BY fldProjectsImagesPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllProjectsImages() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjectsImages ORDER BY fldProjectsImagesPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countProjectsImages() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblProjectsImages";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findProjectsImages($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblProjectsImages WHERE fldProjectsImagesID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteProjectsImages($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblProjectsImages WHERE fldProjectsImagesID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
