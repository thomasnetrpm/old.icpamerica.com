<?
   
class FAQ{
   
    function addFAQ($faq){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($faq->description);
		$name = addslashes($faq->name);
		
	
        $query="INSERT INTO ".$table_prefix."_tblfaq SET ".
            "fldFAQTitle='$name',".
			"fldFAQsDescription='$description'";				
        $adb->query($query);
        return true;
    }
	
	function updateFAQ($faq) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($faq->description);
		$name = addslashes($faq->name);
		
		
			$query="UPDATE ".$table_prefix."_tblfaq SET ".
				 "fldFAQTitle='$name',".
				"fldFAQsDescription='$description'".
           		 " WHERE fldFAQID=$faq->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblfaq";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblfaq ORDER BY fldFAQID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBySearch($search) {
		global $adb;
		global $table_prefix;
		
		//$query = "SELECT * FROM ".$table_prefix."_tblfaq WHERE   ORDER BY fldFAQID";
		$query = "SELECT * FROM ".$table_prefix."_tblfaq WHERE MATCH (fldFAQTitle,fldFAQsDescription) AGAINST ('".$search."' IN BOOLEAN MODE)";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countFAQ() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblfaq";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findFAQ($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblfaq WHERE fldFAQID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteFAQ($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblfaq WHERE fldFAQID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
