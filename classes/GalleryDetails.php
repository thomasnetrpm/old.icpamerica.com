<?

class GalleryDetails{
     
    function addGalleryDetails($gallery,$image){
        global $adb;
		global $table_prefix;
		

		$name = addslashes($gallery->name);
		
        $query="INSERT INTO ".$table_prefix."_tblGalleryDetails SET ".           			
			"fldGalleryDetailsImage='$image',". 
			"fldGalleryDetailsCategoryId='$gallery->category_id',".
			"fldGalleryDetailsGalleryId='$gallery->gallery_id',".
			"fldGalleryDetailsPosition='$gallery->position',".
			"fldGalleryDetailsName='$name'"; 						
        $adb->query($query);
        return true;
    }
	
	function updateGalleryDetails($gallery,$image) {
		 global $adb;
		 global $table_prefix;
		 

		$name = addslashes($gallery->name);
		
		 if($image != "") {
		
			$query="UPDATE ".$table_prefix."_tblGalleryDetails SET ".
           "fldGalleryDetailsName='$name',". 		
		   "fldGalleryDetailsPosition='$gallery->position',".
		   "fldGalleryDetailsImage='$image'".
		    " WHERE fldGalleryDetailsID=$gallery->Id";
		 } else {
			 $query="UPDATE ".$table_prefix."_tblGalleryDetails SET ".
           "fldGalleryDetailsName='$name',". 					
		   "fldGalleryDetailsPosition='$gallery->position'".
		    " WHERE fldGalleryDetailsID=$gallery->Id";
		 }

	    $adb->query($query);
		
				
		
        return true;
	}
    
	function findAll($pg,$gallery_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGalleryDetails WHERE fldGalleryDetailsGalleryId='$gallery_id' ORDER BY fldGalleryDetailsPosition";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll($galery_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGalleryDetails WHERE fldGalleryDetailsGalleryId='$galery_id' ORDER BY fldGalleryDetailsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllGalleryDetails() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGalleryDetails ORDER BY fldGalleryDetailsPosition";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countGalleryDetails($gallery_id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGalleryDetails WHERE fldGalleryDetailsGalleryId='$gallery_id'";		
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findGalleryDetails($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblGalleryDetails WHERE fldGalleryDetailsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteGalleryDetails($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblGalleryDetails WHERE fldGalleryDetailsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
