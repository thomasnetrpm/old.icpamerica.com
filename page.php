<?php
# EXECUTE YOUR CSS
$css = 'page';

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

if(isset($_POST['submit_contact'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$phone 		= trim($_POST['phone']);
	$email 		= trim($_POST['email']);

	if ( ($firstname=='') || ($lastname=='') || ($phone=='') || ($email=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_contactus.php");

		$_POST = sanitize($_POST);
		$contact = $_POST;
		settype($contact,'object');
		Contact::addContact($contact);

		$msg_success = "Contact us form submitted.";
	}
}

if(isset($_POST['submit_support'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$company 	= trim($_POST['company']);
	$email 		= trim($_POST['email']);
	$distributor= trim($_POST['distributor']);
	$position 	= trim($_POST['position']);
	$fax 		= trim($_POST['fax']);
	$country 	= trim($_POST['country']);
	
	if ( ($firstname=='') || ($lastname=='') || ($company=='') || ($email=='') || ($distributor=='') || ($position=='') || ($fax=='') || ($country=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_support.php");
		$msg_success = "Support form submitted.";
	}
}

if(isset($_POST['submit_customquote'])) {
	$firstname 	= trim($_POST['firstname']);
	$lastname 	= trim($_POST['lastname']);
	$email 		= trim($_POST['email']);
	$product_name = trim($_POST['product_name']);
	$phone 		= trim($_POST['phone']);
	
	if ( ($firstname=='') || ($lastname=='') || ($email=='') || ($product_name=='') || ($phone=='') ) {
		$msg_error = 'Please complete information below.';
	} else {
		require_once("includes/mailform/process_customquote.php");
		$msg_success = "Custom Quote form submitted.";
	}
}
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li class="active"><?=$pageName?></li>
    </ul>

    	<?php
    	if (isset($pageID)) {
			if ($pageTitle=='services') { $pageName = 'Industrial Computer Services'; }
    		?>
			<h1><?=$pageName?></h1>

			<? if(isset($msg_error)): ?>
		    <div class="alert alert-error">
		    	<button type="button" class="close" data-dismiss="alert">&times;</button>
		    	<?=$msg_error?>
		    </div>
			<? endif; ?>

			<? if(isset($msg_success)): ?>
		    <div class="alert alert-info">
		    	<button type="button" class="close" data-dismiss="alert">&times;</button>
		    	<?=$msg_success?>
		    </div>
			<? endif; ?>

			<p><?=$pageDesc?></p>

			<?php
			if ($pageTitle=='contact us') {
				?>

					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({ 
					    portalId: '549477',
					    formId: '0957f721-712d-4adc-9cad-ce6c5dc149dd'
					  });
					</script>

				<?php
			}
			if ($pageTitle=='support') {
				?>

					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({ 
					    portalId: '549477',
					    formId: '25a03400-a0e5-474d-b317-13a8c6a2facb'
					  });
					</script>

				<?php
			}
			?>
			<?php
			if ($pageTitle=='custom quote') {
				?>

					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({ 
					    portalId: '549477',
					    formId: '5fe93b56-dda0-4df5-8f22-f9577c46394f'
					  });
					</script>

				<?php
			}
			?>
    		<?php
    	} else {
    		// Show 404 page
    		include "includes/pages/404.php";
    	}
    	?>
	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b22acdb2-b960-43a7-a566-b857f357f33e"});</script>
<? endblock(); ?>