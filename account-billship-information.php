<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

if(isset($_SESSION['client_id'])) {
	$client_id = $_SESSION['client_id'];		
} else {
	header("Location: index.php");
}

if(isset($_POST['update_billship'])) {

    $bill = Billing::findBillingClient($client_id);
    $billing_id = $bill->fldBillingID;

    $ship = Shipping::findShippingClient($client_id);
    $shipping_id = $ship->fldShippingID;

    if ($billing_id > 0) {
        $_POST['Id'] = $billing_id;
        $_POST = sanitize($_POST);
        $client = $_POST;
        settype($client,'object');

        Billing::updateBilling($client);
    } else {
        $_POST = sanitize($_POST);
        $client = $_POST;
        settype($client,'object');
        
        Billing::addBilling($client);
    }

    if ($shipping_id > 0) {
        $_POST['Id'] = $shipping_id;
        $_POST = sanitize($_POST);
        $client = $_POST;
        settype($client,'object');
        
        Shipping::updateShipping($client); 
    } else {
        $_POST = sanitize($_POST);
        $client = $_POST;
        settype($client,'object');
        
        Shipping::addShipping($client); 
    }

    $sucess = "Your Billing/Shipping Information Successfully Save";
}


$client     = Client::findClient($client_id);
$billing    = Billing::findBillingClient($client_id);
$shipping   = Shipping::findShippingClient($client_id);

$bill_firstname = ($billing->fldBillingFirstName)? $billing->fldBillingFirstName: $client->fldClientFirstName;
$bill_lastname  = ($billing->fldBillingLastname)? $billing->fldBillingLastname: $client->fldClientLastname;
$bill_address1  = ($billing->fldBillingAddress)? $billing->fldBillingAddress: $client->fldClientAddress;
$bill_address2  = ($billing->fldBillingAddress1)? $billing->fldBillingAddress1: $client->fldClientAddress1;
$bill_city      = ($billing->fldBillingCity)? $billing->fldBillingCity: $client->fldClientCity;
$bill_state     = ($billing->fldBillingState)? $billing->fldBillingState: $client->fldClientState;
$bill_zip       = ($billing->fldBillingZip)? $billing->fldBillingZip: $client->fldClientZip;
$bill_phone     = ($billing->fldBillingPhoneNo)? $billing->fldBillingPhoneNo: $client->fldClientPhoneNo;
$bill_email     = ($billing->fldBillingEmail)? $billing->fldBillingEmail: $client->fldClientEmail;

$ship_firstname = ($shipping->fldShippingFirstName)? $shipping->fldShippingFirstName: $client->fldClientFirstName;
$ship_lastname  = ($shipping->fldShippingLastname)? $shipping->fldShippingLastname: $client->fldClientLastname;
$ship_address1  = ($shipping->fldShippingAddress)? $shipping->fldShippingAddress: $client->fldClientAddress;
$ship_address2  = ($shipping->fldShippingAddress1)? $shipping->fldShippingAddress1: $client->fldClientAddress1;
$ship_city      = ($shipping->fldShippingCity)? $shipping->fldShippingCity: $client->fldClientCity;
$ship_state     = ($shipping->fldShippingState)? $shipping->fldShippingState: $client->fldClientState;
$ship_zip       = ($shipping->fldShippingZip)? $shipping->fldShippingZip: $client->fldClientZip;
$ship_phone     = ($shipping->fldShippingPhoneNo)? $shipping->fldShippingPhoneNo: $client->fldClientPhoneNo;
$ship_email     = ($shipping->fldShippingEmail)? $shipping->fldShippingEmail: $client->fldClientEmail;
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li>My Account <span class="divider">/</span></li>
      <li class="active">Billing and Shipping Information</li>
    </ul>

    <hgroup>
      <h2>Billing Information</h2>
    </hgroup>

    <? if(isset($_SESSION['update_flag']) && !$_SESSION['update_flag']): ?>
      <br /><br />
      <div id='error_messages'>
        <span>
    	<? foreach($_SESSION['errors'] as $err): ?>
    		<?= $err ?><br />
    	<? endforeach ?>
        <span>
      </div><br />
    <? endif ?>
    
    <? if(isset($_SESSION['message'])): ?>
      <br /><br />
      <div id='messages'>
        <span><?= $_SESSION['message'] ?></span>
      </div><br />
    <? endif ?>

    <form action="" method="post" class="form-horizontal" style="margin-top:20px;">
    <fieldset>
        <?php if(isset($error)) { ?>       
        <div class="alert">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?=$error?>
        </div>
        <? } ?>
        <!-- first name input-->
        <div class="control-group">
            <label class="control-label">First Name</label>
            <div class="controls">
                <input id="firstname" name="firstname" type="text" placeholder="first name" value="<?=$bill_firstname?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- last name input-->
        <div class="control-group">
            <label class="control-label">Last Name</label>
            <div class="controls">
                <input id="lastname" name="lastname" type="text" placeholder="last name" value="<?=$bill_lastname?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- address-line1 input-->
        <div class="control-group">
            <label class="control-label">Address Line 1</label>
            <div class="controls">
                <input id="address" name="address" type="text" placeholder="address 1"  value="<?=$bill_address1?>" class="input-xlarge">
                <p class="help-block">Street address, P.O. box, company name, c/o</p>
            </div>
        </div>
        <!-- address-line2 input-->
        <div class="control-group">
            <label class="control-label">Address Line 2</label>
            <div class="controls">
                <input id="address1" name="address1" type="text" placeholder="address line 2" value="<?=$bill_address2?>" class="input-xlarge">
                <p class="help-block">Apartment, suite , unit, building, floor, etc.</p>
            </div>
        </div>
        <!-- city input-->
        <div class="control-group">
            <label class="control-label">City / Town</label>
            <div class="controls">
                <input id="city" name="city" type="text" placeholder="city" value="<?=$bill_city?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- region input-->
        <div class="control-group">
            <label class="control-label">State</label>
            <div class="controls">
              <select name="state">
                <? 
                $state = State::findAll();
                foreach($state as $states) {
                  if($bill_state == "") {
                    $stateVal = "CA";
                  } else {
                    $stateVal = $bill_state;
                  }
                    if($states->fldStateID == $stateVal) { 
                    ?>
                      <option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
                    <? } else { ?>
                      <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
                    <? } ?>
                <? } ?>
                </select>
                <p class="help-block"></p>
            </div>
        </div>
        <!-- postal-code input-->
        <div class="control-group">
            <label class="control-label">Zip / Postal Code</label>
            <div class="controls">
                <input id="zip" name="zip" type="text" placeholder="zip or postal code" value="<?=$bill_zip?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- telephone input-->
        <div class="control-group">
            <label class="control-label">Telephone Number</label>
            <div class="controls">
                <input id="phone" name="phone" type="text" placeholder="telephone number" value="<?=$bill_phone?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- email input-->
        <div class="control-group">
            <label class="control-label">Email Address</label>
            <div class="controls">
                <input id="email" name="email" type="text" placeholder="email address" value="<?=$bill_email?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
    </fieldset>

    <br><br>
    <hgroup>
      <h2>Shipping Information</h2>
    </hgroup>

    <fieldset>
        <!-- first name input-->
        <div class="control-group">
            <label class="control-label">First Name</label>
            <div class="controls">
                <input id="shipping_firstname" name="shipping_firstname" type="text" placeholder="first name" value="<?=$ship_firstname?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- last name input-->
        <div class="control-group">
            <label class="control-label">Last Name</label>
            <div class="controls">
                <input id="shipping_lastname" name="shipping_lastname" type="text" placeholder="last name" value="<?=$ship_lastname?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- address-line1 input-->
        <div class="control-group">
            <label class="control-label">Address Line 1</label>
            <div class="controls">
                <input id="shipping_address" name="shipping_address" type="text" placeholder="address 1"  value="<?=$ship_address1?>" class="input-xlarge">
                <p class="help-block">Street address, P.O. box, company name, c/o</p>
            </div>
        </div>
        <!-- address-line2 input-->
        <div class="control-group">
            <label class="control-label">Address Line 2</label>
            <div class="controls">
                <input id="shipping_address1" name="shipping_address1" type="text" placeholder="address line 2" value="<?=$ship_address2?>" class="input-xlarge">
                <p class="help-block">Apartment, suite , unit, building, floor, etc.</p>
            </div>
        </div>
        <!-- city input-->
        <div class="control-group">
            <label class="control-label">City / Town</label>
            <div class="controls">
                <input id="shipping_city" name="shipping_city" type="text" placeholder="city" value="<?=$ship_city?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- region input-->
        <div class="control-group">
            <label class="control-label">State</label>
            <div class="controls">
              <select name="shipping_state">
                <? 
                $state = State::findAll();
                foreach($state as $states) {
                  if($ship_state == "") {
                    $stateVal = "CA";
                  } else {
                    $stateVal = $ship_state;
                  }
                    if($states->fldStateID == $stateVal) { 
                    ?>
                      <option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
                    <? } else { ?>
                      <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
                    <? } ?>
                <? } ?>
                </select>
                <p class="help-block"></p>
            </div>
        </div>
        <!-- postal-code input-->
        <div class="control-group">
            <label class="control-label">Zip / Postal Code</label>
            <div class="controls">
                <input id="shipping_zip" name="shipping_zip" type="text" placeholder="zip or postal code" value="<?=$ship_zip?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- telephone input-->
        <div class="control-group">
            <label class="control-label">Telephone Number</label>
            <div class="controls">
                <input id="shipping_phone" name="shipping_phone" type="text" placeholder="telephone number" value="<?=$ship_phone?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- email input-->
        <div class="control-group">
            <label class="control-label">Email Address</label>
            <div class="controls">
                <input id="shipping_email" name="shipping_email" type="text" placeholder="email address" value="<?=$ship_email?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
    </fieldset>

<!--     <input type='hidden' name='fldClientID' value='<?=$client->fldClientID?>'>
    <input type='hidden' name='fldClientUsername' value='<?=$client->fldClientUsername?>'> -->

    <input type='hidden' name='client_id' value='<?=$client_id?>'>
    <button type="submit" name="update_billship" class="save-btn" value="1">Save Changes</button>

    </form>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>


<?
// Clean Session
unset($_SESSION['errors']);
unset($_SESSION['message']);
unset($_SESSION['update_flag']);
?>

<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<style>
.form-horizontal .control-label { text-align: left !important; }
.form-horizontal .control-group { margin-bottom: 5px; }
.form-horizontal input + .help-block { font-size: 11px; margin-top: 5px; }
.save-btn {
	background: #427700;
	border: 0;
	border-radius: 0;
	color: #FFFFFF;
	display: block;
	margin: 10px 0;
	padding: 5px 10px;
}
</style>
<? endblock() ?>

<? startblock('script') ?>
<? endblock() ?>