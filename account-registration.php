<?
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';
include 'classes/Administrator.php';
?>
<?
if(isset($_POST['register'])) {
	$ctr="";

	$username 	= $_POST['username'];
	$password 	= $_POST['password'];
	$password2 	= $_POST['confirm_password'];
	$firstname 	= $_POST['firstname'];
	$lastname 	= $_POST['lastname'];
	$company 	= $_POST['company'];
	$phone 		= $_POST['phone'];
	$address1	= $_POST['address1'];
	$address2	= $_POST['address2'];
	$city 		= $_POST['city'];
	$state 		= $_POST['state'];
	$zip 		= $_POST['zip'];
	$email 		= $_POST['email'];

	if ( ($username=="") || ($password=="") || ($password2=="") || ($firstname=="") || ($lastname=="") ||($phone=="") || ($address1=="") || ($city=="") || ($state=="") || ($zip=="") || ($email=="") ) {
		$error = "Please complete information below."; $ctr = 1;
	} else {
		if ($password != $password2) {$error = "Password and Confirm password do not match."; $ctr = 1; }
		if (Client::countClientEmail($email)==1) {
			$error  = "This Email is Already Registered.";
			$error .= "<br><a href='".$root."forgot-password.html'>Forgot Password?</a>";
			$ctr=1;
		}
		if (Client::countClientUsername($username)==1) {
			// $error = "Username already in used";
			$error  = "This Username is Already Registered.";
			$error .= "<br><a href='".$root."forgot-password.html'>Forgot Password?</a>";
			$ctr=1;
		}
	}


	if($ctr=="") {

	    $mail_content = Pages::findPages(18);
    	$cms_content  = $mail_content->fldPagesDescription;

		//save customer information and send confirmation email
		$_POST = sanitize($_POST);
	    $client = $_POST;
	    settype($client,'object');
		$client_id = Client::addClient($client); 

		// SEND Email to CLIENT ----------------------------------------- START
		require("includes/class.phpmailer.php");
		$mail = new PHPMailer();

		$to = $email;
		$name_from = "sales@icpamerica.com";
		$mail->From = "sales@icpamerica.com";
		$mail->FromName = $name_from;
		$emailto = $to;

		$mail->AddAddress($emailto);
		$mail->AddBCC('test1@dogandrooster.net');
		$mail->IsHTML(true); // set email format to HTML
		$all_html = implode('',file('includes/client_email.php'));

		$all_html = str_replace("%%cms_content%%", $cms_content, $all_html);
		$all_html = str_replace("%%username%%", $username, $all_html);
		$all_html = str_replace("%%password%%", $password, $all_html);
		$all_html = str_replace("%%firstname%%", $firstname, $all_html);
		$all_html = str_replace("%%lastname%%", $lastname, $all_html);			
		$all_html = str_replace("%%email%%", $email, $all_html);

		$mail->Subject = "Welcome to ICP America";

		//$alt_body = $_POST['body'];
		$mail->Body    = $all_html;

		//$mail->AltBody = $alt_body;

		if($mail->Send()){
		 $mail->ClearAddresses();
		}
		// SEND Email to CLIENT ----------------------------------------- END


		// SEND Email to ADMIN ----------------------------------------- START
		$mail->From = $email;
		$mail->FromName = $email;

		/*
		$condSuperAdmin = "fldAdministratorLevel ='1' ";
		$admin = Administrator::findAllAdministratorByCondition($condSuperAdmin);
		foreach ($admin as $adm) {
		  $mail->AddAddress($adm->fldAdministratorEmail); 
		}
		*/
		// $mail->AddAddress('apuglisi@icpamerica.com');
		// $mail->AddAddress('ktinsley@icpamerica.com');
		$mail->AddAddress('sales@icpamerica.com'); 
		$mail->AddBCC('test1@dogandrooster.net');

		$mail->IsHTML(true); // set email format to HTML
		$all_html = implode('',file('includes/client_email.php'));

		$all_html = str_replace("%%cms_content%%", $cms_content, $all_html);
		$all_html = str_replace("%%username%%", $username, $all_html);
		$all_html = str_replace("%%password%%", $password, $all_html);
		$all_html = str_replace("%%firstname%%", $firstname, $all_html);
		$all_html = str_replace("%%lastname%%", $lastname, $all_html);			
		$all_html = str_replace("%%email%%", $email, $all_html);

		$mail->Subject = 'New Client Registration';

		//$alt_body = $_POST['body'];
		$mail->Body    = $all_html;

		//$mail->AltBody = $alt_body;

		if($mail->Send()){
			$mail->ClearAddresses();
		}
		// SEND Email to ADMIN ----------------------------------------- End

		
		$_SESSION['client_id']	 = $client_id;
		$xclient = session_id();
		$condition = "fldTempCartClientID='$xclient'";
		if(TempCart::countTempcartbyCondition($condition)>=1) {

			//change the client id
			TempCart::updateTempcartClient($_SESSION['client_id'],$xclient);
			$links = $ROOT_URL.'billing-info.html';
			header("Location: $links");
		} else {				
			$links = $ROOT_URL.'thankyou.html';
			header("Location: $links");		
		}

		
	}

}

$memberType = trim($_REQUEST['type']);
// echo "memberType: ".$memberType."<br>";
?>

<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
    <ul class="breadcrumb">
	    <li class="active">my account</li>
    </ul>

		<h1>My Account <?=($memberType==2)? "- Corporate": "";?></h1>

		<? if(isset($error)): ?>
			<div class="alert alert-error">
	    		<button type="button" class="close" data-dismiss="alert">&times;</button>
	    		<?=$error?>
	    	</div>
		<? endif; ?>



		<form action="" class="myaccount" method="post">
			<table class="table">
				<tbody>
					<tr>
						<td width="20%">Username *</td>
						<td width="80%"><input type="text" name="username" class="span4" required value="<?=$_POST['username']?>" read data-toggle="popover" data-placement="right" data-original-title="Note: " data-content="Username must be at least 6 characters. No spaces."></td>
					</tr>
					<tr>
						<td>Password *</td>
						<td><input type="password" class="span4" name="password" value="" required read data-toggle="popover" data-placement="right" data-original-title="Note: " data-content="Password must be at least 6 characters. No spaces."></td>
					</tr>
					<tr>
						<td>Confirm Password *</td>
						<td><input type="password" class="span4" name="confirm_password" required value="" read data-toggle="popover" data-placement="right" data-original-title="Note: " data-content="Password entered here must match above password."></td>
					</tr>
					<tr>
						<td>First Name *</td>
						<td><input type="text" class="span6" name="firstname" required value="<?=$_POST['firstname']?>"></td>
					</tr>
					<tr>
						<td>Last Name *</td>
						<td><input type="text" class="span6" name="lastname" required value="<?=$_POST['lastname']?>"></td>
					</tr>
					<tr>
						<td>Company Name</td>
						<td><input type="text" class="span6" name="company" value="<?=$_POST['company']?>"></td>
					</tr>
					<tr>
						<td>Telephone Number *</td>
						<td><input type="text" class="span5" name="phone" value="<?=$_POST['phone']?>" required read data-toggle="popover" data-placement="right" data-original-title="Note: " data-content="Telephone number must only contain numbers."></td>
					</tr>
					<tr>
						<td>Address 1 *</td>
						<td><input type="text" class="span5" name="address1" value="<?=$_POST['address1']?>" required></td>
					</tr>
					<tr>
						<td>Address 2</td>
						<td><input type="text" class="span5" name="address2" value="<?=$_POST['address2']?>"></td>
					</tr>
					<tr>
						<td>City *</td>
						<td><input type="text" class="span4" name="city" value="<?=$_POST['city']?>" required></td>
					</tr>
					<tr>
						<td>State *</td>
						<td>
							<select name="state" required>
							<? 
							$state = State::findAll();
							foreach($state as $states) {
								$stateVal = "CA";
							// if($bill_state == "") {
							// 	$stateVal = "CA";
							// } else {
							// 	$stateVal = $bill_state;
							// }
								if($states->fldStateID == $stateVal) { 
								?>
									<option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
								<? } else { ?>
									<option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
								<? } ?>
							<? } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Zip Code *</td>
						<td><input type="text" class="span4" name="zip" value="<?=$_POST['zip']?>" required></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td>Email *</td>
						<td><input type="email" class="span5" name="email" required value="<?=$_POST['email']?>" read data-toggle="popover" data-placement="right" data-original-title="Note: " data-content="Email used must be in the correct format."></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;(* Fields Required)</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<input type="hidden" name="member_type" value="<?=$memberType?>">
							<? /* <button type="submit" class="submit-btn" name="register">Register</button> */ ?>
							<button class="btn_submit" type="submit" name="register" value="1">Register</button>
						</td>
					</tr>
					
				</tbody>
			</table>


		</form>

	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<link rel="stylesheet" href="<?=$root?>plugins/popover/css/bootstrap.css">
<? endblock(); ?>

<? startblock('script') ?>
<script src="<?=$root?>plugins/popover/js/bootstrap.min.js"></script>
<script>
	$('.menunav .mn3 a').addClass('on');
</script>

<script>
	$(function(){
	    $('*[data-toggle="popover"]').popover({
	        trigger: 'hover'
	    });
	});
</script>
<? endblock(); ?>