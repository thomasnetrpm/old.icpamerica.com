<?
   
class Glossary{
   
    function addGlossary($glossary){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($glossary->description);
		$name = addslashes($glossary->name);
		
	
        $query="INSERT INTO ".$table_prefix."_tblGlossary SET ".
            "fldGlossaryTitle='$name',".
			"fldGlossarysDescription='$description'";				
        $adb->query($query);
        return true;
    }
	
	function updateGlossary($glossary) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($glossary->description);
		$name = addslashes($glossary->name);
		
		
			$query="UPDATE ".$table_prefix."_tblGlossary SET ".
				 "fldGlossaryTitle='$name',".
				"fldGlossarysDescription='$description'".
           		 " WHERE fldGlossaryID=$glossary->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGlossary";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGlossary ORDER BY fldGlossaryID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllBySearch($search) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGlossary WHERE MATCH (fldGlossaryTitle,fldGlossarysDescription) AGAINST ('".$search."' IN BOOLEAN MODE)";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countGlossary() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblGlossary";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findGlossary($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblGlossary WHERE fldGlossaryID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteGlossary($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblGlossary WHERE fldGlossaryID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
