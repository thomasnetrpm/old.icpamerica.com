<?

class Pages{
   
    function addPages($pages){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($pages->content);
		$name = addslashes($pages->name);		
		
		$date = date('Y-m-d');
		
		// $meta_title = addslashes($pages->meta_title);
		// $meta_keywords = addslashes($pages->meta_keywords);
		// $meta_description = addslashes($pages->meta_description);
			
        $query="INSERT INTO ".$table_prefix."_tblPages SET ".
            "fldPagesName='$name',".									
			"fldPagesDateModified='$date',".
			// "fldPagesMetaTitle='$meta_title',".
			// "fldPagesMetaDescription='$meta_description',".
			// "fldPagesMetaKeywords='$meta_keywords',".			
            "fldPagesDescription='$content'";
        $adb->query($query);
        return true;
    }
	
	function updatePages($pages) {
		 global $adb;
		 global $table_prefix;
		 
		$name = addslashes($pages->name);
		$content = addslashes($pages->content);
		$custom1 = addslashes($pages->custom1);
		$custom2 = addslashes($pages->custom2);
		
		$date = date('Y-m-d');
		
		// $meta_title = addslashes($pages->meta_title);
		// $meta_keywords = addslashes($pages->meta_keywords);
		// $meta_description = addslashes($pages->meta_description);
		
		$query  = "UPDATE ".$table_prefix."_tblPages SET ".
			"fldPagesName='$name', ".
			"fldPagesDateModified='$date', ";
			// "fldPagesDescription='$content', ";
		if ($custom1) { $query .= "fldPagesContent1='$custom1', "; }
		if ($custom2) { $query .= "fldPagesContent2='$custom2', "; }
			// "fldPagesMetaDescription='$meta_description',".
			// "fldPagesMetaKeywords='$meta_keywords',".			
		// $query .= "fldPagesMetaTitle='$meta_title' ".
		$query .= "fldPagesDescription='$content' ".
      		"WHERE fldPagesID=$pages->Id";
	    $adb->query($query);
        return true;
	}
	
	
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPages";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPages ORDER BY fldPagesName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countPages() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPages";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findPages($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPages WHERE fldPagesID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deletePages($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblPages WHERE fldPagesID='$id'";
        $adb->query($query);
        return true;
    }
    
    function findPageByTitle($title){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPages WHERE LOWER(fldPagesName) = '".$title."' ";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

	// SEARCH BY KEYWORD
	function countPageByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		// echo $query = "(SELECT fldProductsId as _id, fldProductsName as _name, fldProductsURL as url FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%".$keyword."%' OR fldProductsCode LIKE '%".$keyword."%' OR fldProductsOverview LIKE '%".$keyword."%' OR fldProductsFeatures LIKE '%".$keyword."%' OR fldProductsTechspecs LIKE '%".$keyword."%' OR fldProductsOrderinfo LIKE '%".$keyword."%' OR fldProductMetaTitle LIKE '%".$keyword."%') 
		//            UNION (SELECT fldCategoryID as _id, fldCategoryName as _name, fldCategoryURL as url FROM ".$table_prefix."_tblCategory WHERE fldCategoryName LIKE '%".$keyword."%' OR fldCategoryDescription LIKE '%".$keyword."%' OR fldCategoryMetaTitle LIKE '%".$keyword."%') 
		//            UNION (SELECT fldPagesID as _id, fldPagesName as _name, fldPagesTitle as url FROM ".$table_prefix."_tblPages WHERE fldPagesName LIKE '%".$keyword."%' OR fldPagesDescription LIKE '%".$keyword."%' OR fldPagesMetaTitle LIKE '%".$keyword."%')";

		$query = "(SELECT fldProductsId as _id, fldProductsName as _name, fldProductsOverview as _overview, fldProductsURL as _url FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%".$keyword."%' OR fldProductsCode LIKE '%".$keyword."%' OR fldProductsOverview LIKE '%".$keyword."%' OR fldProductsFeatures LIKE '%".$keyword."%' OR fldProductsTechspecs LIKE '%".$keyword."%' OR fldProductsOrderinfo LIKE '%".$keyword."%' OR fldProductMetaTitle LIKE '%".$keyword."%') 
		           UNION (SELECT fldPagesID as _id, fldPagesName as _name, fldPagesDescription as _overview, fldPagesTitle as _url FROM ".$table_prefix."_tblPages WHERE (fldPagesName LIKE '%".$keyword."%' OR fldPagesDescription LIKE '%".$keyword."%' OR fldPagesMetaTitle LIKE '%".$keyword."%') AND (fldPagesID<>8) )";
		$result = $adb->query($query.$pg);
		return $result->db_num_rows();
	}
	
	function displayPageByKeyword($keyword) {
		global $adb;
		global $table_prefix;
		
		$query = "(SELECT fldProductsId as _id, fldProductsName as _name, fldProductsOverview as _overview, fldProductsURL as _url FROM ".$table_prefix."_tblProducts WHERE fldProductsName LIKE '%".$keyword."%' OR fldProductsCode LIKE '%".$keyword."%' OR fldProductsOverview LIKE '%".$keyword."%' OR fldProductsFeatures LIKE '%".$keyword."%' OR fldProductsTechspecs LIKE '%".$keyword."%' OR fldProductsOrderinfo LIKE '%".$keyword."%' OR fldProductMetaTitle LIKE '%".$keyword."%') 
		           UNION (SELECT fldPagesID as _id, fldPagesName as _name, fldPagesDescription as _overview, fldPagesTitle as _url FROM ".$table_prefix."_tblPages WHERE (fldPagesName LIKE '%".$keyword."%' OR fldPagesDescription LIKE '%".$keyword."%' OR fldPagesMetaTitle LIKE '%".$keyword."%') AND (fldPagesID<>8) )";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
   
// _seoadmin/
	function countPagesSEO() {
		global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblPages WHERE isSEO='1'";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function findAllSEO($pg) {
		global $adb;
		global $table_prefix;

		$query = "SELECT * FROM ".$table_prefix."_tblPages WHERE isSEO='1' ORDER BY fldPagesID ASC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

	function updatePagesMeta($meta) {
		global $adb;
		global $table_prefix;
		
		$metatitle 		= addslashes($meta->meta_title);
		$metakeywords 	= addslashes($meta->meta_keywords);
		$metadescription= addslashes($meta->meta_description);

		$query = "UPDATE ".$table_prefix."_tblPages SET 
				fldPagesMetaTitle='$metatitle', 
				fldPagesMetaKeywords='$metakeywords', 
				fldPagesMetaDescription='$metadescription' 
				WHERE fldPagesID=$meta->Id";
	    $adb->query($query);
        return true;
	}

	function removeSEO($page_id) {
		global $adb;
		global $table_prefix;

		$query = "UPDATE ".$table_prefix."_tblPages SET isSEO='0' WHERE fldPagesID='$page_id'";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

}
?>