<?
     
class PhotoGallery{
   
    function addPhotoGallery($photo,$image,$image1){
        global $adb;
		global $table_prefix;
		
		$name = addslashes($photo->name);
	
        $query="INSERT INTO ".$table_prefix."_tblPhotoGallery SET ".
            "fldPhotoGalleryName='$name',".	
			"fldPhotoGalleryThumb='$image1',".	
            "fldPhotoGalleryImage='$image'";
        $adb->query($query);
        return true;
    }
	
	function updatePhotoGallery($photo,$image,$image1) {
		 global $adb;
		 global $table_prefix;
		 
		$name = addslashes($photo->name);
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblPhotoGallery SET ".		
				 "fldPhotoGalleryName='$name'".	          
           		 " WHERE fldPhotoGalleryID=$photo->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblPhotoGallery SET ".
				 "fldPhotoGalleryName='$name',".
	            "fldPhotoGalleryImage='$image'".
           		 " WHERE fldPhotoGalleryID=$photo->Id";
		}
	    $adb->query($query);
		
		if($image1 != "") {
			$query="UPDATE ".$table_prefix."_tblPhotoGallery SET ".				
	            "fldPhotoGalleryThumb='$image1'".
           		 " WHERE fldPhotoGalleryID=$photo->Id";
		}
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPhotoGallery";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPhotoGallery ORDER BY fldPhotoGalleryID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countPhotoGallery() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPhotoGallery";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findPhotoGallery($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPhotoGallery WHERE fldPhotoGalleryID='$id'";

        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deletePhotoGallery($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblPhotoGallery WHERE fldPhotoGalleryID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
