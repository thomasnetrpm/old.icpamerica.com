<?
   
class ClassesEvents{
   
    function addClassesEvents($events){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($events->description);
		$subtitle = addslashes($events->subtitle);
		$name = addslashes($events->name);
		$date = date('Y-m-d');
        $query="INSERT INTO ".$table_prefix."_tblClassesEvents SET ".
            "fldClassesEventsName='$name',".
			"fldClassesEventsDescription='$description',".	
			"fldClassesEventsSubTitle='$subtitle',".
            "fldClassesEventsDate='$date'";
        $adb->query($query);
        return true;
    }
	
	function updateClassesEvents($events) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($events->description);
		$name = addslashes($events->name);
		$subtitle = addslashes($events->subtitle);
		
			$query="UPDATE ".$table_prefix."_tblClassesEvents SET ".
				"fldClassesEventsName='$name',".
				"fldClassesEventsDescription='$description',".	
				"fldClassesEventsSubTitle='$subtitle',".
				"fldClassesEventsDate='$events->date'".
           		 " WHERE fldClassesEventsID=$events->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesEvents";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesEvents ORDER BY fldClassesEventsDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countClassesEvents() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesEvents";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findClassesEvents($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblClassesEvents WHERE fldClassesEventsID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findLatestClassesEvents(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblClassesEvents ORDER BY fldClassesEventsDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteClassesEvents($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblClassesEvents WHERE fldClassesEventsID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
