<?
class Contact{
   
    function addContact($contact){
        global $adb;
		global $table_prefix;
	
		$fname = addslashes($contact->firstname);
		$lname = addslashes($contact->lastname);
		$company = addslashes($contact->company);
		$phone = addslashes($contact->phone);
		$fax = addslashes($contact->fax);
		$email = addslashes($contact->email);
		$address1 = addslashes($contact->address1);
		$address2 = addslashes($contact->address2);
		$city = addslashes($contact->city);
		$state = addslashes($contact->state);
		$zip = addslashes($contact->zip);
		$find = addslashes($contact->find);
		$comment = addslashes($contact->comment);
		
        $query = "INSERT INTO ".$table_prefix."_tblContact SET ".
            "fldContactFirstName='$fname', ".
            "fldContactLastName='$lname', ".
			"fldContactCompany='$company', ".
			"fldContactPhoneNo='$phone', ".
			"fldContactFax='$fax', ".
			"fldContactEmail='$email', ".
			"fldContactAddress1='$address1', ".
			"fldContactAddress2='$address2', ". 
			"fldContactCity='$city', ". 
			"fldContactState='$state', ". 
			"fldContactZip='$zip', ". 
			"fldContactHowDidYouFindUs='$find', ".
			"fldContactMessage='$comment', ".
			"fldContactDate=NOW() ";
			// die();
        $adb->query($query);
        return mysql_insert_id();
    }
	

	function updateContact($contact) {
		global $adb;
		global $table_prefix;
		
		$fname = addslashes($contact->firstname);
		$lname = addslashes($contact->lastname);
		$company = addslashes($contact->company);
		$phone = addslashes($contact->phone);
		$fax = addslashes($contact->fax);
		$email = addslashes($contact->email);
		$address1 = addslashes($contact->address1);
		$address2 = addslashes($contact->address2);
		$city = addslashes($contact->city);
		$state = addslashes($contact->state);
		$zip = addslashes($contact->zip);
		$find = addslashes($contact->find);
		$comment = addslashes($contact->comment);
		
        $query = "UPDATE ".$table_prefix."_tblContact SET ".
            "fldContactFirstName='$fname', ".
            "fldContactLastName='$lname', ".
			"fldContactCompany='$company', ".
			"fldContactPhoneNo='$phone', ".
			"fldContactFax='$fax', ".
			"fldContactEmail='$email', ".
			"fldContactAddress1='$address1', ".
			"fldContactAddress2='$address2', ". 
			"fldContactCity='$city', ". 
			"fldContactState='$state', ". 
			"fldContactZip='$zip', ". 
			"fldContactHowDidYouFindUs='$find', ".
			"fldContactMessage='$comment' ".
           	"WHERE fldContactID=$contact->Id";

/*
		$name = addslashes($contact->name);
		$company = addslashes($contact->company);
		$job_title = addslashes($contact->job_title);
		$email = addslashes($contact->email);
		$address = addslashes($contact->address);
		$city = addslashes($contact->city);
		$state = addslashes($contact->state);
		$zip = addslashes($contact->zip);
		$phone = addslashes($contact->phone);
		$fax = addslashes($contact->fax);
		$website = addslashes($contact->website);
		$interest = addslashes($contact->interest);
		$email_updates = addslashes($contact->email_updates);		
		$message = addslashes($contact->comments);
		
			$query="UPDATE ".$table_prefix."_tblContact SET ".
			  "fldContactName='$name',". 
			"fldContactCompany='$company',". 
			"fldContactJobTitle='$job_title',". 
			"fldContactAddress='$address',". 
			"fldContactCity='$city',". 
			"fldContactState='$state',". 
			"fldContactZip='$zip',". 
			"fldContactPhoneNo='$phone',".
			"fldContactFax='$fax',".
			"fldContactWebsite='$website',".
			"fldContactInterest='$interest',".
			"fldContactEmailUpdates='$email_updates',".
			"fldContactMessage='$message',".
			"fldContactEmail='$email'". 
           		 " WHERE fldContactID=$contact->Id";
*/

	    $adb->query($query);
        return true;
	}
	
	
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblContact ORDER BY fldContactID DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblContact";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countContact() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblContact";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findEmail($email){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblContact WHERE fldContactEmail='$email'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findContact($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblContact WHERE fldContactID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	   
    function deleteContact($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblContact WHERE fldContactID='$id'";
        $adb->query($query);
        return true;
    }
    
 
   
}
?>
