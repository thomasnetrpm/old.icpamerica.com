<?
   
class ClassesSchedule{
   
    function addClassesSchedule($schedule){
        global $adb;
		global $table_prefix;
		
		$title = addslashes($schedule->title);
		$date = addslashes($schedule->dateSchedule);
		$time = addslashes($schedule->timeSchedule);

        $query="INSERT INTO ".$table_prefix."_tblClassesSchedule SET ".
            "fldClassesEventsID='$schedule->event_id',".
			"fldClassesScheduleTitle='$title',".
			"fldClassesScheduleDate='$date',".	
			"fldClassesScheduleTime='$time'";
        $adb->query($query);
        return true;
    }
	
	function updateClassesSchedule($schedule) {
		 global $adb;
		 global $table_prefix;
		 
		$title = addslashes($schedule->title);
		$date = addslashes($schedule->dateSchedule);
		$time = addslashes($schedule->timeSchedule);
		
			$query="UPDATE ".$table_prefix."_tblClassesSchedule SET ".
				"fldClassesScheduleTitle='$title',".
				"fldClassesScheduleDate='$date',".	
				"fldClassesScheduleTime='$time'".
           		 " WHERE fldClassesScheduleID=$schedule->Id";
	    $adb->query($query);
        return true;
	}
    
	function findAll($pg,$id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule WHERE fldClassesEventsID='$id'";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule ORDER BY fldClassesScheduleDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	function displayAllByEvents($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule WHERE fldClassesEventsID='$id' ORDER BY fldClassesScheduleID";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	
	
	function countClassesSchedule($id) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule WHERE fldClassesEventsID='$id'";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findClassesSchedule($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule WHERE fldClassesScheduleID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findLatestClassesSchedule(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblClassesSchedule ORDER BY fldClassesScheduleDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deleteClassesSchedule($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblClassesSchedule WHERE fldClassesScheduleID='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
