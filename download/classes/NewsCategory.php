<?
   
class NewsCategory{
   
    function addNewsCategory($newscat){
        global $adb;
		global $table_prefix;
		
		$description = addslashes($newscat->description);
		$name = addslashes($newscat->name);
		
	
        $query = "INSERT INTO ".$table_prefix."_tblNewsCategory SET ".
            "nc_name='$name',".
			"nc_description='$description',".
            "nc_status='1'";
        $adb->query($query);
        return true;
    }
	
	function countNewsCategory() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsCategory WHERE nc_status=1 ";
		$result = $adb->query($query);
		return $result->db_num_rows();
	}

	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNewsCategory ORDER BY nc_id ASC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}

    function findNewsCategory($categoryid){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblNewsCategory WHERE nc_id = '$categoryid' ";
        $result=$adb->query($query);
        return $result->fetch_object();
    }

	function updateNewsCategory($newscategory) {
		global $adb;
		global $table_prefix;
		 
		$name = addslashes($newscategory->name);
		$description = addslashes($newscategory->description);
		
		$query = "UPDATE ".$table_prefix."_tblNewsCategory SET ".
			"nc_name='$name', ".
            "nc_description='$description' ".
       		"WHERE nc_id=$newscategory->Id";
	    $adb->query($query);
        return true;
	}

    function deleteNewsCategory($id){
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblNewsCategory WHERE nc_id='$id'";
        $adb->query($query);
        return true;
    }

/*
	function updateNews($news,$image) {
		 global $adb;
		 global $table_prefix;
		 
		$description = addslashes($news->description);
		$name = addslashes($news->name);

		$url 			= addslashes(trim($news->url));
		$metatitle 		= addslashes($news->metatitle);
		$metadesc 		= addslashes($news->metadesc);
		$metakeywords 	= addslashes($news->metakeywords);
		
		if($image == "") { 
			$query="UPDATE ".$table_prefix."_tblNews SET ".
				"fldNewsTitle='$name', ".
				"fldNewsSEOURL='$url', ".
				"fldNewsDescription='$description', ".					
	            "fldNewsDate='$news->date_news', ".
	            "fldNewsMetaTitle='$metatitle', ".
	            "fldNewsMetaDescription='$metadesc', ".
	            "fldNewsMetaKeywords='$metakeywords' ".
           		"WHERE fldNewsID=$news->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblNews SET ".
				"fldNewsTitle='$name', ".
				"fldNewsSEOURL='$url', ".
				"fldNewsDescription='$description', ".
				"fldNewsImage='$image', ".
	            "fldNewsDate='$news->date_news', ".
	            "fldNewsMetaTitle='$metatitle', ".
	            "fldNewsMetaDescription='$metadesc', ".
	            "fldNewsMetaKeywords='$metakeywords' ".
           		"WHERE fldNewsID=$news->Id";
		}
	    $adb->query($query);
				
        return true;
	}
    
	
	function displayAll($newsid) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews WHERE fldNewsID != '$newsid' ORDER BY fldNewsDate DESC";

		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllThumb() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews  ORDER BY fldNewsDate DESC";

		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllNews($start,$end) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT $start,$end";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllLatest() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT 3";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	  function displayLatestNews(){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	 function findNewsLatest(){
        global $adb;
		global $table_prefix;
		
        echo $query = "SELECT * FROM ".$table_prefix."_tblNews ORDER BY fldNewsDate DESC LIMIT 1";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
*/
}
?>
