<?

class Press{
     
    function addPress($press,$file){
        global $adb;
		global $table_prefix;
		
		$content = addslashes($press->content);		
		$name = addslashes($press->name);
		
		
        $query="INSERT INTO ".$table_prefix."_tblPress SET ".           			
				
			"fldPressName='$name',".		
			"fldPressFile='$file',".
			"fldPressDate='$press->date',".
            "fldPressDescription='$content'";
        $adb->query($query);
        return true;
    }
	
	function updatePress($press,$file) {
		 global $adb;
		 global $table_prefix;
		 
		$content = addslashes($press->content);		
		$name = addslashes($press->name);
		
		
		if($image == "") {
			$query="UPDATE ".$table_prefix."_tblPress SET ".
         	"fldPressName='$name',".		
			"fldPressDate='$press->date',".
            "fldPressDescription='$content'".
            " WHERE fldPressId=$press->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblPress SET ".
        		"fldPressName='$name',".		
			"fldPressFile='$file',".
			"fldPressDate='$press->date',".
            "fldPressDescription='$content'".
            " WHERE fldPressId=$press->Id";
		}
	    $adb->query($query);
		
		
        return true;
	}
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPress ORDER BY fldPressDate DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPress ORDER BY fldPressDate DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayFirst4() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPress ORDER BY fldPressDate DESC LIMIT 4";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAllPress() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPress ORDER BY fldPressName";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countPress() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblPress";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
    function findPressByPosition($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPress WHERE fldPressCategory='$id' ORDER BY fldPressDate DESC";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	function findPress($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblPress WHERE fldPressId='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
   
    function deletePress($id){        
        global $adb;
        global $table_prefix;
		
        $query = "DELETE FROM ".$table_prefix."_tblPress WHERE fldPressId='$id'";
        $adb->query($query);
        return true;
    }
    
   
   
}
?>
