<?  
class Careers{
   
    
    function addCareers($careers,$resume){
        global $adb;
		global $table_prefix;
		
		$firstname = addslashes($careers->firstname);
		$lastname = addslashes($careers->lastname);
		$middlename = addslashes($careers->middlename);
		$message = addslashes($careers->message);
		$phone = addslashes($careers->phone);
				
		$date = date('Y-m-d');
		
        $query="INSERT INTO ".$table_prefix."_tblCareers SET ".
			"fldCareersLastname='$lastname',". 
			"fldCareersFirstName='$firstname',". 
			"fldCareersEmail='$careers->email_address',".			
			"fldCareersMiddleName='$middlename',".
			"fldCareersMessage='$message',".
			"fldCareersPhoneNo='$phone',".		
			"fldCareersResume='$resume',".		
			"fldCareersRegDate='$date'";             
        $adb->query($query);
        return mysql_insert_id();
    }
	
	function updateCareers($careers,$resume) {
		 global $adb;
		 global $table_prefix;
		
		$firstname = addslashes($careers->firstname);
		$lastname = addslashes($careers->lastname);
		$middlename = addslashes($careers->middlename);
		$message = addslashes($careers->message);
		$phone = addslashes($careers->phone);
		
		if($resume == "") {
			$query="UPDATE ".$table_prefix."_tblCareers SET ".
			 "fldCareersLastname='$lastname',". 
			"fldCareersFirstName='$firstname',". 
			"fldCareersEmail='$careers->email_address',".			
			"fldCareersMiddleName='$middlename',".
			"fldCareersMessage='$message',".
			"fldCareersPhoneNo='$phone'".		             
            " WHERE fldCareersID=$careers->Id";
		} else {
			$query="UPDATE ".$table_prefix."_tblCareers SET ".
			 "fldCareersLastname='$lastname',". 
			"fldCareersFirstName='$firstname',". 
			"fldCareersEmail='$careers->email_address',".			
			"fldCareersMiddleName='$middlename',".
			"fldCareersMessage='$message',".
			"fldCareersPhoneNo='$phone',".		
			"fldCareersResume='$resume'".		             
            " WHERE fldCareersID=$careers->Id";
		}
	    $adb->query($query);
        return true;
	}
	
		
    
	function findAll($pg) {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCareers ORDER BY fldCareersID DESC";
		$result = $adb->query($query.$pg);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function displayAll() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCareers ORDER BY fldCareersID DESC";
		$result = $adb->query($query);
		$testi = array();
		while($row=$result->fetch_object()){
			$testi[]=$row;
		}
		return $testi;
	}
	
	function countCareers() {
		global $adb;
		global $table_prefix;
		
		$query = "SELECT * FROM ".$table_prefix."_tblCareers";
		$result = $adb->query($query);
		return $result->db_num_rows();
		
	}
	
	
    function findCareers($id){
        global $adb;
		global $table_prefix;
		
        $query = "SELECT * FROM ".$table_prefix."_tblCareers WHERE fldCareersID='$id'";
        $result=$adb->query($query);
        return $result->fetch_object();
    }
	
	
	
	   
    function deleteCareers($id){        
        global $adb;
		global $table_prefix;
        
        $query = "DELETE FROM ".$table_prefix."_tblCareers WHERE fldCareersID='$id'";
        $adb->query($query);
        return true;
    }
    
    
}
?>
