<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";
?>
<?php
$filename = basename($_SERVER['PHP_SELF']);

$prodURL = trim($_REQUEST['url']);

if (substr($prodURL, -12) == 'default.html') { // Category / Subcategory

    // CATEGORY
    // Check if its main category
    $cat = Category::findByURL($prodURL);
    $catID = $cat->fldCategoryID;

    if ($catID >= 1) { // cms_tblCategory
        $cond1 = 'subcategoryID = 0';
        $subcat = Subcategory::findByID($catID, $cond1);
        $showCategory = 1; // Display category table

        // For Medical Products OVERRIDE - Just one category then display product right away
       /* if ($prodURL == 'medical_products/default.html') {
            // echo 'display medical list with products<br>';
            $condition = "AND (fldProductsURL LIKE 'medical_products/%' OR fldProductsURL LIKE 'medical_products/%') ";
            $prod = Products::displayAllByCondition($catID, $condition);
            $countProd = Products::countAllByCondition($catID, $condition);

            $showCategory = '';
            $showProductList = 1; // Display product table
        }*/

    } else { // cms_tblSubcategory
        $subcat = Subcategory::findByURL($prodURL);
        $catID = $subcat->fldCategoryID;
        $subcatID = $subcat->fldSubcategoryID;

        $cond1 = "subcategoryID = '".$subcatID."'";
        // if ($prodURL=='single_board_computer/backplane/default.html') { $cond1 .= " AND subcategory2ID IS NULL"; } // For Backplane only
        if ($prodURL=='backplane/default.html') { $cond1 .= " AND subcategory2ID IS NULL"; } // For Backplane only
        $subcat = Subcategory::findByID($catID, $cond1);
        
        $countSubcat = Subcategory::countByID($catID, $cond1);
        $countNull = Subcategory::countNullByID($catID, $cond1);

        if ($countNull >= 1) { // Category list
            // echo 'display sub-category list with products<br>';
            $showCategoryList = 1; // Display subcategory list and products
        } else {
            if ($countSubcat >= 1) { // Category list
                // echo 'sub-category list<br>';
                $showCategory = 1; // Display subcategory table
            } else { // Products list

                // echo 'product list<br>';
                $showProductList = 1; // Display product table
                $condition = "AND fldProductsURL LIKE '".substr($prodURL, 0, -12)."%'";

                // This is for video_capture / IVC only - Start
                if (substr($prodURL, 0, -12)=='accessories/video_capture/') {
                    $condition = "AND (fldProductsURL LIKE 'accessories/video_capture/%' OR fldProductsURL LIKE 'accessories/IVC/%') ";
                }
                // This is for video_capture / IVC only - End

                // This is for BACKPLANE only - Start
                // echo 'substr produrl: '.substr($prodURL, 0, 9).'<br>';
                if (substr($prodURL, 0, 9)=='backplane') {
                    $countcat = count(explode('/', $prodURL));
                    // echo 'countcat: '.$countcat.'<br>';
                    // echo 'subcatID: '.$subcatID.'<br>';
                    // if (in_array($subcatID, array('90', '195', '194'), true)) { // for PCI/PICOe, PISA and ISA Backplanes
                    //     echo $subcatID." was found in array \n";
                    // }
                    if ($countcat==3) {
                        // $cond1 = "subcategory2ID = '".$subcatID."'";
                        // $subcat = Subcategory::findByID($catID, $cond1);
        
                        // $showCategory = 1; // Display subcategory table
                        
                        if ($subcatID == '194') { // for backplanes / ISA Backplanes
                            $condition .= "AND fldProductsSubcategory2ID='$subcatID'";
                        } else {
                            $cond1 = "subcategory2ID = '".$subcatID."'";
                            $subcat = Subcategory::findByID($catID, $cond1);
            
                            $showCategory = 1; // Display subcategory table
                        }
                    } elseif ($countcat==4) {
                        $condition .= "AND fldProductsSubcategory3ID='$subcatID'";
                    }
                }
                // This is for BACKPLANE only - End

                $prod = Products::displayAllByCondition($catID, $condition);
                $countProd = Products::countAllByCondition($catID, $condition);

                /*
                // This is for BACKPLANE only - Start
                if (substr($prodURL, 0, 9)=='backplane') {
                    $cond1 = "subcategory2ID = '".$subcatID."'";
                    $subcat = Subcategory::findByID($catID, $cond1);
    
                    $showCategory = 1; // Display subcategory table
                }
                // This is for BACKPLANE only - End
                */
            }
        }

    }

    /*
    // For Medical Products OVERRIDE -- Old version - displays Subcategory text and below lists products in one page
    if ($prodURL == 'medical_products/default.html') {
        // echo 'display medical list with products<br>';
        $showCategory = '';
        // $showCategoryList = 1; // Display subcategory list and products
        $showProductList = 1; // Display product table
    }
    */

} else { // Product Details

    if ($filename == "product-detail.php") { $showProductDetails = 1; } // For Product Details page
    
    $prod = Products::findProductByURL($prodURL);
    $productID          = $prod->fldProductsId;
    if (empty($productID)) {
        header('Location: '.$root.'products/default.html');
        exit();
    }

    $productName        = $prod->fldProductsName;
    $productImage       = $prod->fldProductsImage;
    $productCode        = $prod->fldProductsCode;
    $productPDF         = $prod->fldProductsPDF;
    $productUM          = $prod->fldProductsUserManual;
    $productQG          = $prod->fldProductsQuickGuide;
    $productOverview    = $prod->fldProductsOverview;
    $productInfo        = $prod->fldProductsInformation;
    $productDesc        = $prod->fldProductsDescription;
    $productFeatures    = $prod->fldProductsFeatures;
    $productTechspecs   = $prod->fldProductsTechspecs;
    $productOrderinfo   = $prod->fldProductsOrderinfo;
    $productMetaTitle   = $prod->fldProductMetaTitle;
    $productMetaDesc    = $prod->fldProductMetaDescription;

    $catID              = $prod->fldProductsMainCategoryID;
    $subcatID           = $prod->fldProductsSubcategoryID;
    $subcat2ID          = $prod->fldProductsSubcategory2ID;

    // Tiered Pricing
    if ($_SESSION['client_type'] == 1) { $productPrice = $prod->fldProductsPrice1; $productPriceText = $prod->fldProductsPrice1Text; } 
    elseif ($_SESSION['client_type'] == 2) { $productPrice = $prod->fldProductsPrice2; $productPriceText = $prod->fldProductsPrice2Text; } 
    elseif ($_SESSION['client_type'] == 3) { $productPrice = $prod->fldProductsPrice3; $productPriceText = $prod->fldProductsPrice3Text; }

}

$categoryID = $catID;
$cat = Category::findCategory($categoryID);
$category_name = $cat->fldCategoryName;
$category_url = $cat->fldCategoryURL;
$category_desc = $cat->fldCategoryDescription;
$category_metatitle = $cat->fldCategoryMetaTitle;
$category_metadesc = $cat->fldCategoryMetaDescription;
$pageMetatitle = ($category_metatitle)? $category_metatitle: '';
$pageMetadesc = ($category_metadesc)? $category_metadesc: '';

$subcategoryID = $subcatID;
$subcategory = Subcategory::findSubcategory($subcategoryID);
$subcategory_name = $subcategory->fldSubcategoryName;
$subcategory_url = $subcategory->fldSubcategoryURL;
$subcategory_desc = $subcategory->fldSubcategoryDescription;
$subcategory_metatitle = $subcategory->fldSubcategoryMetaTitle;
$subcategory_metadesc = $subcategory->fldSubcategoryMetaDescription;
$pageMetatitle = ($subcategory_metatitle)? $subcategory_metatitle: $pageMetatitle;
$pageMetadesc = ($subcategory_metadesc)? $subcategory_metadesc: $pageMetadesc;

$subcategory2ID = $subcat2ID;
$subcategory2 = Subcategory::findSubcategory($subcategory2ID);
$subcategory2_name = $subcategory2->fldSubcategoryName;
$subcategory2_url = $subcategory2->fldSubcategoryURL;
$subcategory2_desc = $subcategory2->fldSubcategoryDescription;
$subcategory2_metatitle = $subcategory2->fldSubcategoryMetaTitle;
$subcategory2_metadesc = $subcategory2->fldSubcategoryMetaDescription;
$pageMetatitle = ($subcategory2_metatitle)? $subcategory2_metatitle: $pageMetatitle;
$pageMetadesc = ($subcategory2_metadesc)? $subcategory2_metadesc: $pageMetadesc;

if ($showProductDetails == 1) { $pageMetatitle = $productMetaTitle; $pageMetadesc = $productMetaDesc; }
// if ($showCategory == 1) { $pageMetatitle = $category_metatitle; }


// **** Revised Breadcrumbs
$brk_url = explode('/', $_REQUEST['url']);
$bread_cat1 = trim(str_replace(array('_', '-'), ' ', $brk_url[0]));
    if ($bread_cat1=='backplane') { $category_url = "backplane/default.html"; }
$bread_cat2 = trim(str_replace(array('_', '-'), ' ', $brk_url[1]));
$bread_cat3 = trim(str_replace(array('_', '-'), ' ', $brk_url[2]));
// echo 'bread: '.$bread_cat1.' / '.$bread_cat2.' / '.$bread_cat3.'<br>';

    if ($bread_cat1 == 'LCD products') { $bread_cat1 = 'LCD Monitors, Panel PC and Display Kits'; }
$bread_cat1 = (strpos($bread_cat1,'.html') === false)? $bread_cat1: '';
$bread_cat2 = (strpos($bread_cat2,'.html') === false)? $bread_cat2: '';
$bread_cat3 = (strpos($bread_cat3,'.html') === false)? $bread_cat3: '';
$bread_cat4 = ($productCode)? $productCode: '';
// echo 'bread: '.$bread_cat1.' / '.$bread_cat2.' / '.$bread_cat3.' / '.$bread_cat4.'<br>';


// Pages from CMS > Page Management
$pageTitle      = str_replace('_', ' ', trim($_REQUEST['p']));
if ($pageTitle) {
    $content = Pages::findPageByTitle($pageTitle);
    $pageID     = $content->fldPagesID;
    $pageName   = $content->fldPagesName;
    $pageDesc   = $content->fldPagesDescription;

    $pageMetatitle  = $content->fldPagesMetaTitle;
    $pageMetadesc   = $content->fldPagesMetaDescription;
    $pageMetakey    = $content->fldPagesMetaKeywords;
}

if ($pageMetatitle == "") { $pageMetatitle = "ICP America"; }

// For other pages
$filename = basename($_SERVER['PHP_SELF']);
switch ($filename) {
    case 'news.php':
        $pageMetatitle = "News | ICP America";
        $pageMetadesc   = "ICP America�s Monthly News. Newsletter Page, New Products and EOL (end-of-life) Notices of industrial PC products, computer peripherals and accessories.";
        break;
    case 'account.php':
        // $pageMetatitle = "Login | ICP America";
        $pageMetatitle  = "Sign In - Member Login | ICP America";
        $pageMetadesc   = "Login or Create Account. ICP America Members Area. Use your Username and Password to log in to the Members Area.";
        break;
    case 'account-registration.php':
        // $pageMetatitle = "Registration | ICP America";
        $pageMetatitle  = "New Member Registration & Signup | ICP America";
        $pageMetadesc   = "By making an account with our store, you will be able to move over the checkout process faster, store various shipping addresses, view and track your orders in your account and many more.";
        break;
    case 'account-information.php':
        $pageMetatitle = "My Account | ICP America";
        break;
    case 'account-billship-information.php':
        $pageMetatitle = "Edit Billing and Shipping Information | ICP America";
        break;
    case 'product-category.php':
        $pageMetatitle = "Industrial Computers and Computer Products from ICP America";
        $pageMetadesc   = "ICP America industrial PC product categories like Single Board Computers, LCD & Panel PC, Computer Chassis,  Medical Products and other Computer Accessories.";
        break;
    case 'account-history.php':
        $pageMetatitle = "Account History | ICP America";
        break;
    case 'shopping_cart.php':
        $pageMetatitle = "Shopping Cart | ICP America";
        break;
    case 'billing_information.php':
        $pageMetatitle = "Shipping and Billing Information | ICP America";
        break;
    case 'forgot-password.php':
        $pageMetatitle = "Forgot Password | ICP America";
        break;
}
?>
<!doctype html>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<?php if ($pageMetaAuthor) { ?>
<meta name="author" content="<?=$pageMetaAuthor?>">
<? } ?>
<?php if ($pageMetakey) { ?>
<meta name="keyword" content="<?=$pageMetakey?>">
<? } ?>
<?php if ($pageMetadesc) { ?>
<meta name="description" content='<?=$pageMetadesc?>'>
<? } ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="oMzJkYw8s3z4JSvDK-HIv0jIp6hpc0qmrCiCyD8K16w" />﻿
<link rel="stylesheet" type="text/css" media="print" href="<?=$root?>assets/css/print.css">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<link href="<?=$root?>assets/css/rpm-responsive-style.css?=v=68" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?=emptyblock('head');?>
<script src="<?=$root?>assets/js/modernizr.js"></script>

<?php /* <script src="lightbox/js/jquery-1.10.2.min.js"></script> */ ?>
<script src="<?=$root?>plugins/lightbox/js/lightbox-2.6.min.js"></script>
<link href="<?=$root?>plugins/lightbox/css/lightbox.css" rel="stylesheet" />

</head>
<body>

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"QcIFg1awAe00w8", domain:"icpamerica.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=QcIFg1awAe00w8" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<div class="notify">
    This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix" style="position:relative;margin-top:-22px;">
    <form action="<?=$root?>search/default.html" method="post" id="search-panel">
        <div class="inner-wrap">
            <ul class="unstyled clearfix">
            <? if(isset($_SESSION['logged_in'])) { ?>
                <li>Welcome Back <?=$_SESSION['client_firstname']?></li>
                <li><a href='<?=$root?>logout.php'>Logout</a></li>
                <li><span class="utility-email">Email: info@icpamerica.com</span><a class="utility-phone" href="tel:1-877-293-2000">Call: 1-877-293-2000</a></li>        
                <li>
                <div class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account</a>
                    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <div><a href="<?=$root?>account-information.html">My Account</a></div>
                        <div><a href="<?=$root?>account-history.html">Purchase History</a></div>
                        <div><a href="<?=$root?>faqs/default.html">FAQs</a></div>
                    </div>
                </div>
                <a class="m-menu" href="#"><img src="http://www.icpamerica.com/assets/images/ico-nav.svg" alt=""></a>
                </li>
                <? } else { ?>
                    <li><span class="utility-email">Email: info@icpamerica.com</span><a class="utility-phone" href="tel:1-877-293-2000">Call: 1-877-293-2000</a></li>
                    <li><a href="http://info.icpamerica.com/sign-up-for-our-newsletter">Sign Up For Our Newsletter</a><a class="m-menu" href="#"><img src="assets/images/ico-nav.svg" alt=""></a> <a class="m-menu" href="#"><img src="http://www.icpamerica.com/assets/images/ico-nav.svg" alt=""></a></li>
                <? } ?> 
                <li>
                    <input type="text" name="top_keyword" value="" placeholder="Type your keywords...">
                    <input type="submit" name="top_search" value="&nbsp;">
                </li>
                <li>
                    <a href="http://www.facebook.com/icpa1" target="_blank"><img src="<?=$root?>assets/images/icon-facebook.png" width="24" alt=""></a> 
                    <a href="http://www.linkedin.com/company/icp-america-inc." target="_blank"><img src="<?=$root?>assets/images/icon-linkedin.png" width="24" alt=""></a> 
                    <a href="http://twitter.com/icpamerica" target="_blank"><img src="<?=$root?>assets/images/icon-twitter.png" width="24" alt=""></a> 
                    <a href="https://plus.google.com/100124929247060898549" rel="publisher" target="_blank"><img src="<?=$root?>assets/images/icon-google_plus.png" width="24" alt=""></a>
                </li>
            </ul>
        </div><!-- inner-wrap end -->
    </form>
    <header class="site-header">
        <div class="inner-wrap">
            <a id="hdr-logo" href="<?=$root?>" title="go back to Homepage"></a>
            <button type="button" class="media-btn btn">
                <i class="icon-align-justify"></i>
            </button>

            <?php include "includes/pages/top-nav.php"; ?>
        </div><!-- inner-wrap end -->
    </header>
</div>
<!--/ HEADER -->
<div class="wrap content" >
    <a href="http://icpamerica.com/custom_quote/default.html" class="custom-quote"></a>

    <?=emptyblock('content');?>

    <script type="text/javascript">
        (function(){
            var pageUrl = window.location.pathname;
              switch (pageUrl) {
                case '/products/single_board_computers/default.html':
                    $('.panel-pc').prepend('<p>A single board computer, or an SBC, is a fully functioning computer manufactured on a single circuit board. It is created with input/output, memory, and microprocessors, as well as additional features commonly required of complete and fully automated computers. </p><p>Developed for educations and demonstration systems, single board computers are often used as embedded computer controllers. Because it offers centralized functions in one individual printed circuit board, the convenience of a single board computer is unparalleled. </p><p>Single board computers, unlike common desktops, do not depend on expansion slots for expansion or peripheral operations. Certain single board systems are manufactured to connect to a backplane, allowing for an expansion of their system. </p><p>Because they reduce the overall number of required circuit boards and eliminate extraneous bus driver circuits and connecters, single board computers stand to reduce a system&rsquo;s total cost. Made possible by boosting the density of integrated circuits, single board computers eliminate many issues typically associated with common desktop computers. By centralizing the entirety of a computer&rsquo;s functions in one board, a much more compact, convenient overall system is achieved. </p>');
                    $('.panel-pc').append('<p>There are two common categories of single board configuration: no slots, and slot support. Embedded single board computers do not require provision for plug-in cards and are typically employed for machine control, kiosk and gaming applications. They are smaller and ideal for industrial application. Single board computers are utilized in a rackmount chassis format and can be embedded within a range of devices to provide interfacing and control. </p><p>Though they are produced in smaller quantities than traditional boards, single board computers are typically more compact, more power efficient and lighter than multi-board computers. This is due to the high integration levels, decreased connector counts, and reduced component counts of SBCs. </p><p>As with other applications, there are varying types and standards of SBCs presently available on the market. One common assortment of single board computers employs standardized form factors typically used with a backplane enclosure; these include: PICMG, PXI, VMEbus, CompactPCI, and VXI. With Intel PCs, the interface security and intelligence is attached to a plug-in board, which is then implanted into a passive or active backplane. The end result produces a system similar to that of a system built with motherboards, except here the backplane is what determines the slot configuration. </p><p>Other single board computers employ circuit boards that can be stacked, thus negating the need for a backplane. These boards use expansion hardware and have connectors that allow for the stacking of circuit boards. SBCs that are stack-types typically provide memory on plug-cards. SBC stack-types include: EPIC, EBX, PCI-104, PC/104, and PC/104-Plus. </p><p>ICP America has maintained its status as one of America&rsquo;s premiere computer innovative designers for over 25 years. Our SBC boards, introduced in the late 1980s, have found a home within the Automation &amp; Industrial Control industries. Because of their unique and efficient features, our SBC boards continue to provide niche markets with the exact results they&rsquo;re looking for. </p><p>For more information on our single board computers, please refer to our catalog, or <a href="http://info.icpamerica.com/contact-us">contact us</a> today. </p>');
                    break;

                case '/products/LCD_products/default.html':
                    $('.panel-pc').prepend('<p>A liquid crystal display, or LCD, is an electronic visual display that utilizes the light modulating characteristics of liquid crystals. LCDs are typically used to demonstrate fixed or arbitrary images with little information content. These images are comprised of a great number of small pixels, compared to other displays that often have larger or greater elements. </p></p>LCDs are employed in a variety of applications, including televisions, aircraft cockpit displays, and computer monitors, among many more. Liquid crystal displays have succeeded cathode ray tube (CRT) displays in almost all applications, rendering the latter obsolete. Liquid crystal display screens are also much more energy efficient and offer a safer disposal process than those of CRTs. </p><p>Liquid crystal displays provide consumers with a range of veritable benefits. Defined by their agile and compact composition, LCDs offer no geometric distortion, and provide clear, sharp images free of bleeding (when operated at native resolution). Additionally, LCDs do not require a great deal of power to function, so their consumption of energy is limited. Because of this, only a small amount of heat is emitted during its operation.</p><p>A Panel PC is commonly accompanied by an LCD and is assimilated into the same enclosure as the motherboard. Typically, panel PCs utilize touch screens for ease of use and are often panel mounted.<p></p>Industrial PCs are often and commonly used for data procurement and/or process control. At times, industrial PCs will simply be used as a front-end to a different control computer within a distributed processing climate. Industrial PCs offer expansion options, reliability, and compatibility that typical consumer PCs are incapable of. </p>');
                    $('.panel-pc').append('<p>At ICP America, we are proud suppliers of industrial and embedded panels PCs, rackmount LCD drawers, industrial LCD displays, and touch screens. Our industrial panel PCs are equipped with a rugged, durable exterior and have IP 65 aluminum front panels. Our embedded all-in-one HMI panel PCs are supplied with a full computing system, and come in a wide variety suitable for your needs.</p><p>Our workstation series features an integrated LCD, with a durable membrane keypad and waterproof panels, making it ideal for withstanding a range of conditions. Our industrial LCD displays include an adaptable, fully functional line of IP64/65 compliant, vibration and shock resistant displays. Additionally, ICP America\'s touch screen series features screens that range from 6.5 to 19 inches, as well as four and five wire resistive touch. </p><p>ICP America has maintained its status as one of America\'s premiere computer innovative designers for over 25 years. We have come to be known within the industry as the \"Computer System Solution Provider, \" and pride ourselves on our ability to help customers differentiate themselves from their competition. </p><p>ICP America has been the producer of forward-thinking, innovative designs, including our versatile line of LCD products and panel PCs, since the 1980s. We challenge our engineers to create computer products that will exceed consumer expectations and meet the anticipated needs of our customers. </p><p>For more information on our LCD products and panel PCs, please refer to our catalog, or <a href="http://info.icpamerica.com/contact-us">contact us</a> today.</p> ');
                    break;

                case '/products/chassis/default.html':
                    $('.panel-pc').prepend('<p>A computer chassis, also known as a computer case or base unit, is an enclosure containing the majority of a computer\'s core components. Typically, a chassis is built from steel or aluminum, though some home-built computer cases have been manufactured from materials like glass or wood. While some mistakenly refer to the chassis as a computer\'s hard drive or CPU, it is its own distinct entity. </p><p>A computer chassis is commonly comprised of sheet metal enclosures, which provide drive bays and a power supply. Rear panels help to contain peripheral connectors that protrude from expansion slots and the computer\'s motherboard. Power status is indicated by switches or buttons, LEDS and a \"reset\" button. Most chassis are also equipped with dust filters, placed before the air intake fans.</p> <p>Computer chassis come in a varied range of sizes, known commonly as form factors. The computer\'s motherboard is typically what determines the shape and size of a chassis, as it is the largest piece of most computer systems. Layout and internal dimensions are often specified with cases, though form factors of blade servers and rackmount chassis are often equipped with exact external dimensions, as they must fit within specific enclosures. </p><p>Power supply unit mounting points vary between models and cases, though the top and bottom of the chassis are often the most common locations for these units. Drive bays are often found on the front side of the chassis, as well as LEDs and logistical buttons. Vents are located in various locations on the chassis, allowing for cool air to access the case and hot air to be released. Larger vents may allow for cooling fans. </p>');
                    $('.panel-pc').append('<p>At ICP America, our high quality computer chassis catalog includes: rackmount chassis, compact wall mount chassis, embedded chassis, video systems and bare bone and embedded systems. Our line of rackmount chassis include: 1U rackmount service chassis, 2U rackmount server chassis, 4U rackmount server chassis, and 5U rackmount server chassis. They are industrial made and feature cost-efficient power supply. </p><p>Our embedded chassis are manufactured to support a variety of embedded single board computers, and range from EBC-2100 to EBC-3680. Our compact wall mount chassis are designed specifically for use with industrial motherboards, providing added efficiency to any system. They range in slot size from 3 to 10 different slots. </p><p>ICP America also provides customers with a full line of video systems, including DVR and video capture systems. Our video systems offer fanless, embedded systems for maximum efficiency. Our bare bone and embedded systems also feature fully integrated fanless embedded systems. </p><p>ICP America has maintained its status as one of America\'s premiere computer innovative designers for over 25 years. We provide our customers with the computer solutions they need to make simple modifications to standard products. Because of their unique and efficient features, our computer chassis and video systems continue to provide consumers with the exact quality and results they\'re looking for. </p><p>For more information on our computer chassis and video systems, please refer to our catalog, or <a href="http://info.icpamerica.com/contact-us">contact us</a> today. </p>');
                    break;
                case '/products/medical_products/default.html':
                    $('.panel-pc').prepend('<p>A liquid crystal display, or LCD, is an electronic visual display that utilizes the light modulating characteristics of liquid crystals. LCDs are typically used to demonstrate fixed or arbitrary images with little information content. These images are comprised of a great number of small pixels, compared to other displays that often have larger or greater elements. </p><p>LCDs are employed for a variety of operations and functions, including medical applications, aircraft cockpit displays, and computer monitors, among many more. Liquid crystal displays have succeeded cathode ray tube (CRT) displays in almost all applications, rendering the latter obsolete. Liquid crystal display screens are also much more energy efficient and offer a safer disposal process than those of CRTs. </p><p>Liquid crystal displays provide consumers with a range of veritable benefits. Defined by their agile and compact composition, LCDs offer no geometric distortion, and provide clear, sharp images free of bleeding (when operated at native resolution). Additionally, LCDs do not require a great deal of power to function, so their consumption of energy is limited. Because of this, only a small amount of heat is emitted during its operation.</p><p>Because of their high quality display and unparalleled efficiency, LCD displays are ideal for use within the medical field. At ICP America, we are proud suppliers of Medical Grade LCD Displays and Medical Panel PCs. Our Medical Grade LCD Displays include MMD-2213M, MMD-4300C, and MMD-2213CH, among many more varieties. </p>');
                    $('.panel-pc').append('<p>Panel PCs are often accompanied by an LCD, and are assimilated into the same enclosure as a computer\'s motherboard and additional electronic parts. They typically offer touch screens for convenient user interaction, and are commonly panel-mounted. </p><p>At ICP America, Inc, we offer high caliber, Medical Grade LCD Displays and Medical Panel PCs. Our LCD displays offer high brightness and high resolution monitors, and provide exact DICOM calibration. Additionally, they provide excellent display images, ideal for optimal image fusion displays and 3D color rendering. </p><p>Our LCD displays are offered in a wide variety of models and styles, and are equipped with monochrome LCD monitors, varying color displays, and calibration kits. Their smart OSDs offer user-friendly and simple monitor control, and their intelligent display modes offer convenient auto adjustment. </p><p>Like our high quality Medical Grade LCD Displays, our Medical Panel PCs also offer a wide range of exemplary benefits for the medical industry. At ICP America, we offer a complete line of medical grade PC solutions made for patient care. These POC-W22A-H81 screens are equipped with 2M cameras, projected capacitive touchscreens, microphones, and support iRIS-2400. </p><p>ICP America has maintained its status as one of America\'s premiere computer innovative designers for over 25 years. We provide our customers with the computer solutions they need to make simple modifications to standard products. Because of their efficiency, intelligent features, and convenience, our LCD displays and panel PCs are able to offer the medical industry the features and results they need for success. </p><p>For more information on our Medical Grade LCD Displays or Medical Panel PCs, please refer to our catalog, or <a href="http://info.icpamerica.com/contact-us">contact us</a> today. </p>');
            }
        })();
    </script>
   

</div>
<!--/ CONTENT -->
<?php include "includes/footer.php"; ?>
<? /*ph
<div class="wrap footer">
    <footer class="clearfix">
        <span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span> | <span><a href="<?=$root?>site_map/default.html">Site Map</a></span>
        <small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
    </footer>
</div>
*/ ?>
<!--/ FOOTER -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<script src="<?=$root?>assets/js/placeholder.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];  
    _gaq.push(['_setAccount', 'UA-48231208-1']);  
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>﻿
<?=emptyblock('script');?>

<!-- Mobile Nav -->
<script>
$(".m-menu").click(function(){
    $('.menunav ul').toggleClass("m-show");
  });
</script>

<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/549477.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol + "//www.webtraxs.com/trxscript.php' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
_trxid = "icpamerica";
webTraxs();
</script>
<noscript><img src="http://www.webtraxs.com/webtraxs.php?id=icpamerica&st=img" alt=""></noscript>

</body>
</html>