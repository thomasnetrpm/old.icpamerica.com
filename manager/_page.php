<?
$root = '1';

include "includes/config.php";

require_once "_control.php";
require_once "_inheritance.php";

$pageTitle 		= str_replace('_', ' ', trim($_REQUEST['p']));

// echo 'p: '.$_REQUEST['p'];
// echo "<br>";
// echo 'pageTitle: '.$pageTitle;

$content = Pages::findPageByTitle($pageTitle);
$pageID 	= $content->fldPagesID;
$pageName 	= $content->fldPagesName;
$pageDesc 	= $content->fldPagesDescription;

$pageMetatitle	= $content->fldPagesMetaTitle;
$pageMetadesc	= $content->fldPagesMetaDescription;
$pageMetakey	= $content->fldPagesMetaKeywords;

$path = $_SERVER['PHP_SELF'];
$filename = basename($path);
if ($filename == "shopping_cart.php") {
  require_once("includes/shopping_cart.php");
}
// echo 'filename: '.$filename.'<br>';
// echo 'client_id: '.$_SESSION['client_id'].'<br>';
?>

<!doctype html>
<!--[if lte IE 8]><html class="msie no-js" lang="en"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?=$pageMetatitle?></title>
<meta name="description" content="<?=$pageMetadesc?>">
<meta name="keywords" content="<?=$pageMetakey?>">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link href="<?=$root?>assets/images/ico/favicon.ico" rel="shortcut icon">
<link href="<?=$root?>assets/css/strap.css.php?css=<?=$css?>" rel="stylesheet">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?=emptyblock('head');?>
<script src="<?=$root?>assets/js/modernizr.js"></script>
</head>
<body>
<div class="notify">
	This notification indicates, function was successfully initiate!
</div>
<div class="wrap header clearfix">
	<form action="" method="post" id="search-panel">
		<ul class="unstyled clearfix">
			<li><a href='logout.php'>Logout</a></li>
			<li>Email: <?=$SITE_EMAIL?> &nbsp;&nbsp; Call: 1-800-293-2000</li>
			<? if($user=='logged-in') { ?>
			<li>
		    <div class="dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account</a>
			    <div class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			    	<div><a href="#myaccount">My Account</a></div>
			    	<div><a href="#history">Purchase History</a></div>
			    </div>
		    </div>
			</li>
			<? } else { ?>
			<li><a href="#signin">Sign Up</a> <a href="#login">Login</a></li>
			<? } ?>	
			<li><input type="text" name="q" placeholder="Type your keywords..."><input type="submit" name="search" value="&nbsp;"></li>
			<li><a href="#"><img src="assets/images/icon-facebook.png" width="24" alt=""></a> <a href="#"><img src="assets/images/icon-linkedin.png" width="24" alt=""></a> <a href="#"><img src="assets/images/icon-twitter.png" width="24" alt=""></a> <a href="#"><img src="assets/images/icon-google_plus.png" width="24" alt=""></a></li>
		</ul>
	</form>
	<header>
		<a id="hdr-logo" href="#" title="go back to Homepage"></a>
		<button type="button" class="media-btn btn">
			<i class="icon-align-justify"></i>
		</button>

		<?php include "includes/pages/top-nav.php"; ?>

	</header>
</div>
<!--/ HEADER -->
<div class="wrap content">
	<?=emptyblock('content');?>
</div>
<!--/ CONTENT -->
<div class="wrap footer">
	<footer class="clearfix">
		<span>&copy; <?=date('Y');?> ICP America, Inc. All product specifications are subject to change without notice</span> | <span><a href="<?=$root?>privacy_policy/default.html">Privacy Policy</a></span>
		<small><a href="//dogandrooster.com" target="_blank">Website Design by Dog and Rooster, Inc.</a></small>
	</footer>
</div>
<!--/ FOOTER -->
<!-- INT/EXT JAVASCRIPT -->
<script src="<?=$root?>assets/js/bootstrap.min.js"></script>
<script src="<?=$root?>assets/js/plugins.js"></script>
<?=emptyblock('script');?>
</body>
</html>