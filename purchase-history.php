<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

// $client_id = null;

if(isset($_SESSION['client_id'])) {
	$client_id = $_SESSION['client_id'];		
} else {
	header("Location: index.php");
}

$client = Client::findClient( $client_id );
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="#">Home</a> <span class="divider">/</span></li>
      <li>My Account <span class="divider">/</span></li>
      <li class="active">Purchase History</li>
    </ul>

    <hgroup>
      <h2>Purchase History</h2>
    </hgroup>

    This is where you can view your most recent order.

      <table>
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <td colspan='3'>
              Purchase History
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Order No</td>
            <td>Status</td>
            <td>View</td>
          </tr>
          <?php
          $cart = Cart::findAllClient($page, $client->fldClientID);
          foreach ($cart as $cartR) {
            $orderNo = $cartR->fldCartOrderNo;
            $status = $cartR->fldCartStatus;
            $cartID = $cartR->fldCartID;
            ?>
            <tr>
              <td>&bull;</td>
              <td><?=$orderNo?></td>
              <td><?=$status?></td>
              <td><a href='<?=$cartID?>'>View</a></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>


<?
// Clean Session
unset($_SESSION['errors']);
unset($_SESSION['message']);
unset($_SESSION['update_flag']);
?>

<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('headercodes') ?>
<? endblock() ?>

<? startblock('extracodes') ?>
<? endblock() ?>