<?php
# EXECUTE YOUR CSS
$css = 'page';

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

if(isset($_POST['submit_contact'])) {
	require_once("includes/mailform/process_contactus.php");

	$_POST = sanitize($_POST);
	$contact = $_POST;
	settype($contact,'object');
	Contact::addContact($contact);

	$message = "<font color='red'> - Contact us form submitted.</font>";
	// header("Location: thankyou.php");
}

if(isset($_POST['submit_support'])) {
	require_once("includes/mailform/process_support.php");

	$message = "<font color='red'> - Support form submitted.</font>";
	// header("Location: thankyou.php");
}

if(isset($_POST['submit_customquote'])) {
	require_once("includes/mailform/process_customquote.php");

	$message = "<font color='red'> - Custom Quote form submitted.</font>";
}
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
	
	<? include 'includes/sidepanel.php'; ?>
	<!-- End of Side Panel -->

	<section>
  <!--  <ul class="breadcrumb">
	    <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
	    <li class="active"><?=$pageName?></li>
    </ul> -->
		
		<div style="width:100%; float:left; text-align:left; padding:0px 20px 20px 20px;">
    <h1 style="margin-bottom:30px;">Not found <span>:(</span></h1>
    <p style="margin-top:10px;"><b>Sorry, but the page you were trying to view does not exist.</p>
    <ul>
      <li>a mistyped address</li>
      <li>an out-of-date link</li>
    </ul>
    
    <div style="clear:both; line-height:30px; padding:10px;"></div>
    
    <script>
      var GOOG_FIXURL_LANG = (navigator.language || '').slice(0,2),GOOG_FIXURL_SITE = location.host;
    </script>
    <script src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js"></script>
  </div>
		
	</section>
	<!-- End of Content Panel -->

</article>
<? endblock(); ?>



<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<? endblock(); ?>

<? startblock('script') ?>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b22acdb2-b960-43a7-a566-b857f357f33e"});</script>
<? endblock(); ?>