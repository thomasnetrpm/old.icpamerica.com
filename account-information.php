<?php
# EXECUTE YOUR CSS
$css = 'page';

# CHECK USER CREDENDTIALS
$user = $_GET['user'];

# IMPORT YOUR BASE TEMPLATE
include 'manager/page.php';

// $client_id = null;

if(isset($_SESSION['client_id'])) {
	$client_id = $_SESSION['client_id'];		
} else {
	header("Location: index.php");
}

$client = Client::findClient( $client_id );
?>
<? # CONTENTS BLOCK # ?>
<? startblock('content') ?>
<article class="products clearfix">
  
  <? include 'includes/sidepanel.php'; ?>
  <!-- End of Side Panel -->

  <section>
    <ul class="breadcrumb">
      <li><a href="<?=$root?>">Home</a> <span class="divider">/</span></li>
      <li>My Account <span class="divider">/</span></li>
      <li class="active">Account Information</li>
    </ul>

    <hgroup>
      <h2>My Account</h2>
    </hgroup>

    <? if(isset($_SESSION['update_flag']) && !$_SESSION['update_flag']): ?>
      <br /><br />
      <div id='error_messages'>
        <span>
    	<? foreach($_SESSION['errors'] as $err): ?>
    		<?= $err ?><br />
    	<? endforeach ?>
        <span>
      </div><br />
    <? endif ?>
    
    <? if(isset($_SESSION['message'])): ?>
      <br /><br />
      <div id='messages'>
        <span><?= $_SESSION['message'] ?></span>
      </div><br />
    <? endif ?>

    This is where you can edit your account settings.
    <br>
    <a href="<?=$root?>edit-billing-shipping-information.html"><font color=green>Click here to Edit your <b>Billing and Shipping Information</b>.</font></a>

    <form action="save-account-information.php" method="post" class="form-horizontal" style="margin-top:20px;">
    <fieldset>
        <?php if(isset($error)) { ?>       
        <div class="alert">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?=$error?>
        </div>
        <? } ?>
        <!-- username input-->
        <div class="control-group">
            <label class="control-label">Username</label>
            <div class="controls">
                <input id="username" name="username" type="text" value="<?=$client->fldClientUsername?>" id="disabledInput" disabled
                class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- password input-->
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <input id="password" name="password" type="password" placeholder="password" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- confirm password input-->
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <input id="confirm_password" name="confirm_password" type="password" placeholder="confirm password" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>

        <!-- email input-->
        <div class="control-group">
            <label class="control-label">Email Address</label>
            <div class="controls">
                <input id="email" name="email" type="text" placeholder="email address" value="<?=$client->fldClientEmail?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- first name input-->
        <div class="control-group">
            <label class="control-label">First Name</label>
            <div class="controls">
                <input id="firstname" name="firstname" type="text" placeholder="first name" value="<?=$client->fldClientFirstName?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- last name input-->
        <div class="control-group">
            <label class="control-label">Last Name</label>
            <div class="controls">
                <input id="lastname" name="lastname" type="text" placeholder="last name" value="<?=$client->fldClientLastname?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>

        <!-- company input-->
        <div class="control-group">
            <label class="control-label">Company Name</label>
            <div class="controls">
                <input id="company" name="company" type="text" placeholder="company name" value="<?=$client->fldClientCompany?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- telephone input-->
        <div class="control-group">
            <label class="control-label">Telephone Number</label>
            <div class="controls">
                <input id="phone" name="phone" type="text" placeholder="telephone number" value="<?=$client->fldClientPhoneNo?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- address-line1 input-->
        <div class="control-group">
            <label class="control-label">Address Line 1</label>
            <div class="controls">
                <input id="address1" name="address1" type="text" placeholder="address 1"  value="<?=$client->fldClientAddress?>" class="input-xlarge">
                <p class="help-block">Street address, P.O. box, company name, c/o</p>
            </div>
        </div>
        <!-- address-line2 input-->
        <div class="control-group">
            <label class="control-label">Address Line 2</label>
            <div class="controls">
                <input id="address2" name="address2" type="text" placeholder="address line 2" value="<?=$client->fldClientAddress1?>" class="input-xlarge">
                <p class="help-block">Apartment, suite , unit, building, floor, etc.</p>
            </div>
        </div>
        <!-- city input-->
        <div class="control-group">
            <label class="control-label">City / Town</label>
            <div class="controls">
                <input id="city" name="city" type="text" placeholder="city" value="<?=$client->fldClientCity?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
        <!-- region input-->
        <div class="control-group">
            <label class="control-label">State</label>
            <div class="controls">
              <select name="state">
                <? 
                $state = State::findAll();
                foreach($state as $states) {
                  if($client->fldClientState == "") {
                    $stateVal = "CA";
                  } else {
                    $stateVal = $client->fldClientState;
                  }
                    if($states->fldStateID == $stateVal) { 
                    ?>
                      <option value="<?=$states->fldStateID?>" selected="selected"><?=$states->fldStateName?></option>
                    <? } else { ?>
                      <option value="<?=$states->fldStateID?>"><?=$states->fldStateName?></option>
                    <? } ?>
                <? } ?>
                </select>
                <p class="help-block"></p>
            </div>
        </div>

        <!-- postal-code input-->
        <div class="control-group">
            <label class="control-label">Zip / Postal Code</label>
            <div class="controls">
                <input id="zip" name="zip" type="text" placeholder="zip or postal code" value="<?=$client->fldClientZip?>" class="input-xlarge">
                <p class="help-block"></p>
            </div>
        </div>
    </fieldset>
    <input type='hidden' name='fldClientID' value='<?=$client->fldClientID?>'>
    <input type='hidden' name='fldClientUsername' value='<?=$client->fldClientUsername?>'>
    <button type="submit" name="submit_changes" class="save-btn" value="1">Save Changes</button>

    </form>

  </section>
  <!-- End of Content Panel -->

</article>
<? endblock(); ?>


<?
// Clean Session
unset($_SESSION['errors']);
unset($_SESSION['message']);
unset($_SESSION['update_flag']);
?>

<? # CSS & JAVASCRIPT BLOCK # ?>
<? startblock('head') ?>
<style>
.form-horizontal .control-label { text-align: left !important; }
.form-horizontal .control-group { margin-bottom: 5px; }
.form-horizontal input + .help-block { font-size: 11px; margin-top: 5px; }
.save-btn {
	background: #427700;
	border: 0;
	border-radius: 0;
	color: #FFFFFF;
	display: block;
	margin: 10px 0;
	padding: 5px 10px;
}
</style>
<? endblock() ?>

<? startblock('script') ?>
<? endblock() ?>